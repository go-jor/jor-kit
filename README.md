Mattex eMat Kit

[![Language](https://img.shields.io/badge/Language-Go-blue.svg)](https://golang.org/)

## Directory Structure
    ├── api                 # 服务子命令
    ├── docker              # docker-compose 依赖的image
    ├── protobuf            # JOR protobuf file
    └── make                # make run command

## Generate proto file
    make
    
