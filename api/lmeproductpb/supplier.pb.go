// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.33.0
// 	protoc        v5.26.1
// source: lmeproduct/supplier.proto

package lmeproductpb

import (
	jorcommonpb "gitlab.com/go-jor/jor-kit/api/jorcommonpb"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Supplier struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id            int64   `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	PrimaryName   string  `protobuf:"bytes,2,opt,name=primary_name,json=primaryName,proto3" json:"primary_name,omitempty"`
	SecondaryName string  `protobuf:"bytes,4,opt,name=secondary_name,json=secondaryName,proto3" json:"secondary_name,omitempty"`
	Tel           string  `protobuf:"bytes,7,opt,name=tel,proto3" json:"tel,omitempty"`
	Fax           int64   `protobuf:"varint,8,opt,name=fax,proto3" json:"fax,omitempty"`
	Website       int64   `protobuf:"varint,9,opt,name=website,proto3" json:"website,omitempty"`
	Br            float64 `protobuf:"fixed64,10,opt,name=br,proto3" json:"br,omitempty"`
	Remark        string  `protobuf:"bytes,11,opt,name=remark,proto3" json:"remark,omitempty"`
	Status        int32   `protobuf:"varint,12,opt,name=status,proto3" json:"status,omitempty"`
}

func (x *Supplier) Reset() {
	*x = Supplier{}
	if protoimpl.UnsafeEnabled {
		mi := &file_lmeproduct_supplier_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Supplier) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Supplier) ProtoMessage() {}

func (x *Supplier) ProtoReflect() protoreflect.Message {
	mi := &file_lmeproduct_supplier_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Supplier.ProtoReflect.Descriptor instead.
func (*Supplier) Descriptor() ([]byte, []int) {
	return file_lmeproduct_supplier_proto_rawDescGZIP(), []int{0}
}

func (x *Supplier) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *Supplier) GetPrimaryName() string {
	if x != nil {
		return x.PrimaryName
	}
	return ""
}

func (x *Supplier) GetSecondaryName() string {
	if x != nil {
		return x.SecondaryName
	}
	return ""
}

func (x *Supplier) GetTel() string {
	if x != nil {
		return x.Tel
	}
	return ""
}

func (x *Supplier) GetFax() int64 {
	if x != nil {
		return x.Fax
	}
	return 0
}

func (x *Supplier) GetWebsite() int64 {
	if x != nil {
		return x.Website
	}
	return 0
}

func (x *Supplier) GetBr() float64 {
	if x != nil {
		return x.Br
	}
	return 0
}

func (x *Supplier) GetRemark() string {
	if x != nil {
		return x.Remark
	}
	return ""
}

func (x *Supplier) GetStatus() int32 {
	if x != nil {
		return x.Status
	}
	return 0
}

type ReadSupplierRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id   int64                `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Lang jorcommonpb.Language `protobuf:"varint,2,opt,name=lang,proto3,enum=jorcommon.Language" json:"lang,omitempty"`
}

func (x *ReadSupplierRequest) Reset() {
	*x = ReadSupplierRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_lmeproduct_supplier_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ReadSupplierRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ReadSupplierRequest) ProtoMessage() {}

func (x *ReadSupplierRequest) ProtoReflect() protoreflect.Message {
	mi := &file_lmeproduct_supplier_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ReadSupplierRequest.ProtoReflect.Descriptor instead.
func (*ReadSupplierRequest) Descriptor() ([]byte, []int) {
	return file_lmeproduct_supplier_proto_rawDescGZIP(), []int{1}
}

func (x *ReadSupplierRequest) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *ReadSupplierRequest) GetLang() jorcommonpb.Language {
	if x != nil {
		return x.Lang
	}
	return jorcommonpb.Language(0)
}

type ReadSupplierResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data *Supplier `protobuf:"bytes,1,opt,name=data,proto3" json:"data,omitempty"`
}

func (x *ReadSupplierResponse) Reset() {
	*x = ReadSupplierResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_lmeproduct_supplier_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ReadSupplierResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ReadSupplierResponse) ProtoMessage() {}

func (x *ReadSupplierResponse) ProtoReflect() protoreflect.Message {
	mi := &file_lmeproduct_supplier_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ReadSupplierResponse.ProtoReflect.Descriptor instead.
func (*ReadSupplierResponse) Descriptor() ([]byte, []int) {
	return file_lmeproduct_supplier_proto_rawDescGZIP(), []int{2}
}

func (x *ReadSupplierResponse) GetData() *Supplier {
	if x != nil {
		return x.Data
	}
	return nil
}

type ListSuppliersRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	PrimaryName       string                         `protobuf:"bytes,1,opt,name=primary_name,json=primaryName,proto3" json:"primary_name,omitempty"`
	SecondaryName     string                         `protobuf:"bytes,2,opt,name=secondary_name,json=secondaryName,proto3" json:"secondary_name,omitempty"`
	Status            jorcommonpb.NormalStatus       `protobuf:"varint,5,opt,name=status,proto3,enum=jorcommon.NormalStatus" json:"status,omitempty"`
	PaginationRequest *jorcommonpb.PaginationRequest `protobuf:"bytes,6,opt,name=pagination_request,json=paginationRequest,proto3" json:"pagination_request,omitempty"`
	Lang              jorcommonpb.Language           `protobuf:"varint,7,opt,name=lang,proto3,enum=jorcommon.Language" json:"lang,omitempty"`
}

func (x *ListSuppliersRequest) Reset() {
	*x = ListSuppliersRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_lmeproduct_supplier_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ListSuppliersRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ListSuppliersRequest) ProtoMessage() {}

func (x *ListSuppliersRequest) ProtoReflect() protoreflect.Message {
	mi := &file_lmeproduct_supplier_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ListSuppliersRequest.ProtoReflect.Descriptor instead.
func (*ListSuppliersRequest) Descriptor() ([]byte, []int) {
	return file_lmeproduct_supplier_proto_rawDescGZIP(), []int{3}
}

func (x *ListSuppliersRequest) GetPrimaryName() string {
	if x != nil {
		return x.PrimaryName
	}
	return ""
}

func (x *ListSuppliersRequest) GetSecondaryName() string {
	if x != nil {
		return x.SecondaryName
	}
	return ""
}

func (x *ListSuppliersRequest) GetStatus() jorcommonpb.NormalStatus {
	if x != nil {
		return x.Status
	}
	return jorcommonpb.NormalStatus(0)
}

func (x *ListSuppliersRequest) GetPaginationRequest() *jorcommonpb.PaginationRequest {
	if x != nil {
		return x.PaginationRequest
	}
	return nil
}

func (x *ListSuppliersRequest) GetLang() jorcommonpb.Language {
	if x != nil {
		return x.Lang
	}
	return jorcommonpb.Language(0)
}

type ListSuppliersResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Data []*Supplier                `protobuf:"bytes,1,rep,name=data,proto3" json:"data,omitempty"`
	Meta *jorcommonpb.MataPaginator `protobuf:"bytes,2,opt,name=meta,proto3" json:"meta,omitempty"`
}

func (x *ListSuppliersResponse) Reset() {
	*x = ListSuppliersResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_lmeproduct_supplier_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ListSuppliersResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ListSuppliersResponse) ProtoMessage() {}

func (x *ListSuppliersResponse) ProtoReflect() protoreflect.Message {
	mi := &file_lmeproduct_supplier_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ListSuppliersResponse.ProtoReflect.Descriptor instead.
func (*ListSuppliersResponse) Descriptor() ([]byte, []int) {
	return file_lmeproduct_supplier_proto_rawDescGZIP(), []int{4}
}

func (x *ListSuppliersResponse) GetData() []*Supplier {
	if x != nil {
		return x.Data
	}
	return nil
}

func (x *ListSuppliersResponse) GetMeta() *jorcommonpb.MataPaginator {
	if x != nil {
		return x.Meta
	}
	return nil
}

var File_lmeproduct_supplier_proto protoreflect.FileDescriptor

var file_lmeproduct_supplier_proto_rawDesc = []byte{
	0x0a, 0x19, 0x6c, 0x6d, 0x65, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x2f, 0x73, 0x75, 0x70,
	0x70, 0x6c, 0x69, 0x65, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0a, 0x6c, 0x6d, 0x65,
	0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x1a, 0x16, 0x6a, 0x6f, 0x72, 0x63, 0x6f, 0x6d, 0x6d,
	0x6f, 0x6e, 0x2f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22,
	0xe2, 0x01, 0x0a, 0x08, 0x53, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x12, 0x0e, 0x0a, 0x02,
	0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x21, 0x0a, 0x0c,
	0x70, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x0b, 0x70, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4e, 0x61, 0x6d, 0x65, 0x12,
	0x25, 0x0a, 0x0e, 0x73, 0x65, 0x63, 0x6f, 0x6e, 0x64, 0x61, 0x72, 0x79, 0x5f, 0x6e, 0x61, 0x6d,
	0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0d, 0x73, 0x65, 0x63, 0x6f, 0x6e, 0x64, 0x61,
	0x72, 0x79, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x10, 0x0a, 0x03, 0x74, 0x65, 0x6c, 0x18, 0x07, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x03, 0x74, 0x65, 0x6c, 0x12, 0x10, 0x0a, 0x03, 0x66, 0x61, 0x78, 0x18,
	0x08, 0x20, 0x01, 0x28, 0x03, 0x52, 0x03, 0x66, 0x61, 0x78, 0x12, 0x18, 0x0a, 0x07, 0x77, 0x65,
	0x62, 0x73, 0x69, 0x74, 0x65, 0x18, 0x09, 0x20, 0x01, 0x28, 0x03, 0x52, 0x07, 0x77, 0x65, 0x62,
	0x73, 0x69, 0x74, 0x65, 0x12, 0x0e, 0x0a, 0x02, 0x62, 0x72, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x01,
	0x52, 0x02, 0x62, 0x72, 0x12, 0x16, 0x0a, 0x06, 0x72, 0x65, 0x6d, 0x61, 0x72, 0x6b, 0x18, 0x0b,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x72, 0x65, 0x6d, 0x61, 0x72, 0x6b, 0x12, 0x16, 0x0a, 0x06,
	0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x0c, 0x20, 0x01, 0x28, 0x05, 0x52, 0x06, 0x73, 0x74,
	0x61, 0x74, 0x75, 0x73, 0x22, 0x4e, 0x0a, 0x13, 0x52, 0x65, 0x61, 0x64, 0x53, 0x75, 0x70, 0x70,
	0x6c, 0x69, 0x65, 0x72, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69,
	0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x27, 0x0a, 0x04, 0x6c,
	0x61, 0x6e, 0x67, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x13, 0x2e, 0x6a, 0x6f, 0x72, 0x63,
	0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x4c, 0x61, 0x6e, 0x67, 0x75, 0x61, 0x67, 0x65, 0x52, 0x04,
	0x6c, 0x61, 0x6e, 0x67, 0x22, 0x40, 0x0a, 0x14, 0x52, 0x65, 0x61, 0x64, 0x53, 0x75, 0x70, 0x70,
	0x6c, 0x69, 0x65, 0x72, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x28, 0x0a, 0x04,
	0x64, 0x61, 0x74, 0x61, 0x18, 0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x14, 0x2e, 0x6c, 0x6d, 0x65,
	0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x2e, 0x53, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72,
	0x52, 0x04, 0x64, 0x61, 0x74, 0x61, 0x22, 0x87, 0x02, 0x0a, 0x14, 0x4c, 0x69, 0x73, 0x74, 0x53,
	0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12,
	0x21, 0x0a, 0x0c, 0x70, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x70, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4e, 0x61,
	0x6d, 0x65, 0x12, 0x25, 0x0a, 0x0e, 0x73, 0x65, 0x63, 0x6f, 0x6e, 0x64, 0x61, 0x72, 0x79, 0x5f,
	0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0d, 0x73, 0x65, 0x63, 0x6f,
	0x6e, 0x64, 0x61, 0x72, 0x79, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x2f, 0x0a, 0x06, 0x73, 0x74, 0x61,
	0x74, 0x75, 0x73, 0x18, 0x05, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x17, 0x2e, 0x6a, 0x6f, 0x72, 0x63,
	0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x4e, 0x6f, 0x72, 0x6d, 0x61, 0x6c, 0x53, 0x74, 0x61, 0x74,
	0x75, 0x73, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x4b, 0x0a, 0x12, 0x70, 0x61,
	0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x5f, 0x72, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x18, 0x06, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1c, 0x2e, 0x6a, 0x6f, 0x72, 0x63, 0x6f, 0x6d, 0x6d,
	0x6f, 0x6e, 0x2e, 0x50, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x52, 0x11, 0x70, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x69, 0x6f, 0x6e,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x27, 0x0a, 0x04, 0x6c, 0x61, 0x6e, 0x67, 0x18,
	0x07, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x13, 0x2e, 0x6a, 0x6f, 0x72, 0x63, 0x6f, 0x6d, 0x6d, 0x6f,
	0x6e, 0x2e, 0x4c, 0x61, 0x6e, 0x67, 0x75, 0x61, 0x67, 0x65, 0x52, 0x04, 0x6c, 0x61, 0x6e, 0x67,
	0x22, 0x6f, 0x0a, 0x15, 0x4c, 0x69, 0x73, 0x74, 0x53, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72,
	0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x28, 0x0a, 0x04, 0x64, 0x61, 0x74,
	0x61, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x14, 0x2e, 0x6c, 0x6d, 0x65, 0x70, 0x72, 0x6f,
	0x64, 0x75, 0x63, 0x74, 0x2e, 0x53, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x52, 0x04, 0x64,
	0x61, 0x74, 0x61, 0x12, 0x2c, 0x0a, 0x04, 0x6d, 0x65, 0x74, 0x61, 0x18, 0x02, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x18, 0x2e, 0x6a, 0x6f, 0x72, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x4d, 0x61,
	0x74, 0x61, 0x50, 0x61, 0x67, 0x69, 0x6e, 0x61, 0x74, 0x6f, 0x72, 0x52, 0x04, 0x6d, 0x65, 0x74,
	0x61, 0x32, 0xba, 0x01, 0x0a, 0x0f, 0x53, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x53, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x51, 0x0a, 0x0c, 0x52, 0x65, 0x61, 0x64, 0x53, 0x75, 0x70,
	0x70, 0x6c, 0x69, 0x65, 0x72, 0x12, 0x1f, 0x2e, 0x6c, 0x6d, 0x65, 0x70, 0x72, 0x6f, 0x64, 0x75,
	0x63, 0x74, 0x2e, 0x52, 0x65, 0x61, 0x64, 0x53, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x20, 0x2e, 0x6c, 0x6d, 0x65, 0x70, 0x72, 0x6f, 0x64,
	0x75, 0x63, 0x74, 0x2e, 0x52, 0x65, 0x61, 0x64, 0x53, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72,
	0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x54, 0x0a, 0x0d, 0x4c, 0x69, 0x73, 0x74,
	0x53, 0x75, 0x70, 0x70, 0x6c, 0x69, 0x65, 0x72, 0x73, 0x12, 0x20, 0x2e, 0x6c, 0x6d, 0x65, 0x70,
	0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x53, 0x75, 0x70, 0x70, 0x6c,
	0x69, 0x65, 0x72, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x21, 0x2e, 0x6c, 0x6d,
	0x65, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x2e, 0x4c, 0x69, 0x73, 0x74, 0x53, 0x75, 0x70,
	0x70, 0x6c, 0x69, 0x65, 0x72, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x42, 0x12,
	0x5a, 0x10, 0x61, 0x70, 0x69, 0x2f, 0x6c, 0x6d, 0x65, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74,
	0x70, 0x62, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_lmeproduct_supplier_proto_rawDescOnce sync.Once
	file_lmeproduct_supplier_proto_rawDescData = file_lmeproduct_supplier_proto_rawDesc
)

func file_lmeproduct_supplier_proto_rawDescGZIP() []byte {
	file_lmeproduct_supplier_proto_rawDescOnce.Do(func() {
		file_lmeproduct_supplier_proto_rawDescData = protoimpl.X.CompressGZIP(file_lmeproduct_supplier_proto_rawDescData)
	})
	return file_lmeproduct_supplier_proto_rawDescData
}

var file_lmeproduct_supplier_proto_msgTypes = make([]protoimpl.MessageInfo, 5)
var file_lmeproduct_supplier_proto_goTypes = []interface{}{
	(*Supplier)(nil),                      // 0: lmeproduct.Supplier
	(*ReadSupplierRequest)(nil),           // 1: lmeproduct.ReadSupplierRequest
	(*ReadSupplierResponse)(nil),          // 2: lmeproduct.ReadSupplierResponse
	(*ListSuppliersRequest)(nil),          // 3: lmeproduct.ListSuppliersRequest
	(*ListSuppliersResponse)(nil),         // 4: lmeproduct.ListSuppliersResponse
	(jorcommonpb.Language)(0),             // 5: jorcommon.Language
	(jorcommonpb.NormalStatus)(0),         // 6: jorcommon.NormalStatus
	(*jorcommonpb.PaginationRequest)(nil), // 7: jorcommon.PaginationRequest
	(*jorcommonpb.MataPaginator)(nil),     // 8: jorcommon.MataPaginator
}
var file_lmeproduct_supplier_proto_depIdxs = []int32{
	5, // 0: lmeproduct.ReadSupplierRequest.lang:type_name -> jorcommon.Language
	0, // 1: lmeproduct.ReadSupplierResponse.data:type_name -> lmeproduct.Supplier
	6, // 2: lmeproduct.ListSuppliersRequest.status:type_name -> jorcommon.NormalStatus
	7, // 3: lmeproduct.ListSuppliersRequest.pagination_request:type_name -> jorcommon.PaginationRequest
	5, // 4: lmeproduct.ListSuppliersRequest.lang:type_name -> jorcommon.Language
	0, // 5: lmeproduct.ListSuppliersResponse.data:type_name -> lmeproduct.Supplier
	8, // 6: lmeproduct.ListSuppliersResponse.meta:type_name -> jorcommon.MataPaginator
	1, // 7: lmeproduct.SupplierService.ReadSupplier:input_type -> lmeproduct.ReadSupplierRequest
	3, // 8: lmeproduct.SupplierService.ListSuppliers:input_type -> lmeproduct.ListSuppliersRequest
	2, // 9: lmeproduct.SupplierService.ReadSupplier:output_type -> lmeproduct.ReadSupplierResponse
	4, // 10: lmeproduct.SupplierService.ListSuppliers:output_type -> lmeproduct.ListSuppliersResponse
	9, // [9:11] is the sub-list for method output_type
	7, // [7:9] is the sub-list for method input_type
	7, // [7:7] is the sub-list for extension type_name
	7, // [7:7] is the sub-list for extension extendee
	0, // [0:7] is the sub-list for field type_name
}

func init() { file_lmeproduct_supplier_proto_init() }
func file_lmeproduct_supplier_proto_init() {
	if File_lmeproduct_supplier_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_lmeproduct_supplier_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Supplier); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_lmeproduct_supplier_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ReadSupplierRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_lmeproduct_supplier_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ReadSupplierResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_lmeproduct_supplier_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ListSuppliersRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_lmeproduct_supplier_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ListSuppliersResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_lmeproduct_supplier_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   5,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_lmeproduct_supplier_proto_goTypes,
		DependencyIndexes: file_lmeproduct_supplier_proto_depIdxs,
		MessageInfos:      file_lmeproduct_supplier_proto_msgTypes,
	}.Build()
	File_lmeproduct_supplier_proto = out.File
	file_lmeproduct_supplier_proto_rawDesc = nil
	file_lmeproduct_supplier_proto_goTypes = nil
	file_lmeproduct_supplier_proto_depIdxs = nil
}
