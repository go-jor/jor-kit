// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.33.0
// 	protoc        v5.26.1
// source: marketplace/order.proto

package marketplacepb

import (
	jorcommonpb "gitlab.com/go-jor/jor-kit/api/jorcommonpb"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type CommonRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	AccessToken string `protobuf:"bytes,1,opt,name=access_token,json=accessToken,proto3" json:"access_token,omitempty"`
	ShopName    string `protobuf:"bytes,2,opt,name=shop_name,json=shopName,proto3" json:"shop_name,omitempty"`
	Platform    string `protobuf:"bytes,3,opt,name=platform,proto3" json:"platform,omitempty"`
}

func (x *CommonRequest) Reset() {
	*x = CommonRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_marketplace_order_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CommonRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CommonRequest) ProtoMessage() {}

func (x *CommonRequest) ProtoReflect() protoreflect.Message {
	mi := &file_marketplace_order_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CommonRequest.ProtoReflect.Descriptor instead.
func (*CommonRequest) Descriptor() ([]byte, []int) {
	return file_marketplace_order_proto_rawDescGZIP(), []int{0}
}

func (x *CommonRequest) GetAccessToken() string {
	if x != nil {
		return x.AccessToken
	}
	return ""
}

func (x *CommonRequest) GetShopName() string {
	if x != nil {
		return x.ShopName
	}
	return ""
}

func (x *CommonRequest) GetPlatform() string {
	if x != nil {
		return x.Platform
	}
	return ""
}

type UpdateTrackingRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Common          *CommonRequest       `protobuf:"bytes,1,opt,name=common,proto3" json:"common,omitempty"`
	PlatformOrderId string               `protobuf:"bytes,2,opt,name=platform_order_id,json=platformOrderId,proto3" json:"platform_order_id,omitempty"`
	CarrierName     string               `protobuf:"bytes,3,opt,name=carrier_name,json=carrierName,proto3" json:"carrier_name,omitempty"`
	TrackingNos     []string             `protobuf:"bytes,4,rep,name=tracking_nos,json=trackingNos,proto3" json:"tracking_nos,omitempty"`
	TrackingUrls    []string             `protobuf:"bytes,5,rep,name=tracking_urls,json=trackingUrls,proto3" json:"tracking_urls,omitempty"`
	Lang            jorcommonpb.Language `protobuf:"varint,6,opt,name=lang,proto3,enum=jorcommon.Language" json:"lang,omitempty"`
}

func (x *UpdateTrackingRequest) Reset() {
	*x = UpdateTrackingRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_marketplace_order_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateTrackingRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateTrackingRequest) ProtoMessage() {}

func (x *UpdateTrackingRequest) ProtoReflect() protoreflect.Message {
	mi := &file_marketplace_order_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateTrackingRequest.ProtoReflect.Descriptor instead.
func (*UpdateTrackingRequest) Descriptor() ([]byte, []int) {
	return file_marketplace_order_proto_rawDescGZIP(), []int{1}
}

func (x *UpdateTrackingRequest) GetCommon() *CommonRequest {
	if x != nil {
		return x.Common
	}
	return nil
}

func (x *UpdateTrackingRequest) GetPlatformOrderId() string {
	if x != nil {
		return x.PlatformOrderId
	}
	return ""
}

func (x *UpdateTrackingRequest) GetCarrierName() string {
	if x != nil {
		return x.CarrierName
	}
	return ""
}

func (x *UpdateTrackingRequest) GetTrackingNos() []string {
	if x != nil {
		return x.TrackingNos
	}
	return nil
}

func (x *UpdateTrackingRequest) GetTrackingUrls() []string {
	if x != nil {
		return x.TrackingUrls
	}
	return nil
}

func (x *UpdateTrackingRequest) GetLang() jorcommonpb.Language {
	if x != nil {
		return x.Lang
	}
	return jorcommonpb.Language(0)
}

var File_marketplace_order_proto protoreflect.FileDescriptor

var file_marketplace_order_proto_rawDesc = []byte{
	0x0a, 0x17, 0x6d, 0x61, 0x72, 0x6b, 0x65, 0x74, 0x70, 0x6c, 0x61, 0x63, 0x65, 0x2f, 0x6f, 0x72,
	0x64, 0x65, 0x72, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0b, 0x6d, 0x61, 0x72, 0x6b, 0x65,
	0x74, 0x70, 0x6c, 0x61, 0x63, 0x65, 0x1a, 0x16, 0x6a, 0x6f, 0x72, 0x63, 0x6f, 0x6d, 0x6d, 0x6f,
	0x6e, 0x2f, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0x6b,
	0x0a, 0x0d, 0x43, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12,
	0x21, 0x0a, 0x0c, 0x61, 0x63, 0x63, 0x65, 0x73, 0x73, 0x5f, 0x74, 0x6f, 0x6b, 0x65, 0x6e, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x61, 0x63, 0x63, 0x65, 0x73, 0x73, 0x54, 0x6f, 0x6b,
	0x65, 0x6e, 0x12, 0x1b, 0x0a, 0x09, 0x73, 0x68, 0x6f, 0x70, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x73, 0x68, 0x6f, 0x70, 0x4e, 0x61, 0x6d, 0x65, 0x12,
	0x1a, 0x0a, 0x08, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x18, 0x03, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x08, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x22, 0x8b, 0x02, 0x0a, 0x15,
	0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x54, 0x72, 0x61, 0x63, 0x6b, 0x69, 0x6e, 0x67, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x32, 0x0a, 0x06, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x6d, 0x61, 0x72, 0x6b, 0x65, 0x74, 0x70, 0x6c,
	0x61, 0x63, 0x65, 0x2e, 0x43, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73,
	0x74, 0x52, 0x06, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x12, 0x2a, 0x0a, 0x11, 0x70, 0x6c, 0x61,
	0x74, 0x66, 0x6f, 0x72, 0x6d, 0x5f, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x4f, 0x72,
	0x64, 0x65, 0x72, 0x49, 0x64, 0x12, 0x21, 0x0a, 0x0c, 0x63, 0x61, 0x72, 0x72, 0x69, 0x65, 0x72,
	0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x63, 0x61, 0x72,
	0x72, 0x69, 0x65, 0x72, 0x4e, 0x61, 0x6d, 0x65, 0x12, 0x21, 0x0a, 0x0c, 0x74, 0x72, 0x61, 0x63,
	0x6b, 0x69, 0x6e, 0x67, 0x5f, 0x6e, 0x6f, 0x73, 0x18, 0x04, 0x20, 0x03, 0x28, 0x09, 0x52, 0x0b,
	0x74, 0x72, 0x61, 0x63, 0x6b, 0x69, 0x6e, 0x67, 0x4e, 0x6f, 0x73, 0x12, 0x23, 0x0a, 0x0d, 0x74,
	0x72, 0x61, 0x63, 0x6b, 0x69, 0x6e, 0x67, 0x5f, 0x75, 0x72, 0x6c, 0x73, 0x18, 0x05, 0x20, 0x03,
	0x28, 0x09, 0x52, 0x0c, 0x74, 0x72, 0x61, 0x63, 0x6b, 0x69, 0x6e, 0x67, 0x55, 0x72, 0x6c, 0x73,
	0x12, 0x27, 0x0a, 0x04, 0x6c, 0x61, 0x6e, 0x67, 0x18, 0x06, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x13,
	0x2e, 0x6a, 0x6f, 0x72, 0x63, 0x6f, 0x6d, 0x6d, 0x6f, 0x6e, 0x2e, 0x4c, 0x61, 0x6e, 0x67, 0x75,
	0x61, 0x67, 0x65, 0x52, 0x04, 0x6c, 0x61, 0x6e, 0x67, 0x32, 0x65, 0x0a, 0x12, 0x4d, 0x61, 0x72,
	0x6b, 0x65, 0x74, 0x70, 0x6c, 0x61, 0x63, 0x65, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12,
	0x4f, 0x0a, 0x0e, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x54, 0x72, 0x61, 0x63, 0x6b, 0x69, 0x6e,
	0x67, 0x12, 0x22, 0x2e, 0x6d, 0x61, 0x72, 0x6b, 0x65, 0x74, 0x70, 0x6c, 0x61, 0x63, 0x65, 0x2e,
	0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x54, 0x72, 0x61, 0x63, 0x6b, 0x69, 0x6e, 0x67, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x19, 0x2e, 0x6a, 0x6f, 0x72, 0x63, 0x6f, 0x6d, 0x6d, 0x6f,
	0x6e, 0x2e, 0x53, 0x69, 0x6d, 0x70, 0x6c, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65,
	0x42, 0x13, 0x5a, 0x11, 0x61, 0x70, 0x69, 0x2f, 0x6d, 0x61, 0x72, 0x6b, 0x65, 0x74, 0x70, 0x6c,
	0x61, 0x63, 0x65, 0x70, 0x62, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_marketplace_order_proto_rawDescOnce sync.Once
	file_marketplace_order_proto_rawDescData = file_marketplace_order_proto_rawDesc
)

func file_marketplace_order_proto_rawDescGZIP() []byte {
	file_marketplace_order_proto_rawDescOnce.Do(func() {
		file_marketplace_order_proto_rawDescData = protoimpl.X.CompressGZIP(file_marketplace_order_proto_rawDescData)
	})
	return file_marketplace_order_proto_rawDescData
}

var file_marketplace_order_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_marketplace_order_proto_goTypes = []interface{}{
	(*CommonRequest)(nil),              // 0: marketplace.CommonRequest
	(*UpdateTrackingRequest)(nil),      // 1: marketplace.UpdateTrackingRequest
	(jorcommonpb.Language)(0),          // 2: jorcommon.Language
	(*jorcommonpb.SimpleResponse)(nil), // 3: jorcommon.SimpleResponse
}
var file_marketplace_order_proto_depIdxs = []int32{
	0, // 0: marketplace.UpdateTrackingRequest.common:type_name -> marketplace.CommonRequest
	2, // 1: marketplace.UpdateTrackingRequest.lang:type_name -> jorcommon.Language
	1, // 2: marketplace.MarketplaceService.UpdateTracking:input_type -> marketplace.UpdateTrackingRequest
	3, // 3: marketplace.MarketplaceService.UpdateTracking:output_type -> jorcommon.SimpleResponse
	3, // [3:4] is the sub-list for method output_type
	2, // [2:3] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_marketplace_order_proto_init() }
func file_marketplace_order_proto_init() {
	if File_marketplace_order_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_marketplace_order_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CommonRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_marketplace_order_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateTrackingRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_marketplace_order_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_marketplace_order_proto_goTypes,
		DependencyIndexes: file_marketplace_order_proto_depIdxs,
		MessageInfos:      file_marketplace_order_proto_msgTypes,
	}.Build()
	File_marketplace_order_proto = out.File
	file_marketplace_order_proto_rawDesc = nil
	file_marketplace_order_proto_goTypes = nil
	file_marketplace_order_proto_depIdxs = nil
}
