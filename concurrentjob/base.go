package concurrentjob

type JobIdentifier interface {
	Identifier() string
	Message() string
}

type ConcurrentJob interface {
	JobIdentifier
	Run() *JobErr
	AddSubscriber(subsriber chan<- *JobErr)
	GetSubscribers() []chan<- *JobErr
	SetNextJob(newJob ConcurrentJob)
	GetNextJob() ConcurrentJob
}

type RetriableConcurrentJob interface {
	ConcurrentJob
	NeedRetryByNewJob(newJob ConcurrentJob) bool
}

type ConcurrentJobWithErrFormatter interface {
	ConcurrentJob
	GetFinishedErrLogStr(jErr *JobErr) string
}

type BaseConcurrentJob struct {
	subscribers []chan<- *JobErr
	nextJob     chan ConcurrentJob
}

func (j *BaseConcurrentJob) AddSubscriber(subsriber chan<- *JobErr) {
	j.subscribers = append(j.subscribers, subsriber)
}

func (j *BaseConcurrentJob) GetSubscribers() []chan<- *JobErr {
	return j.subscribers
}

func (j *BaseConcurrentJob) SetNextJob(newJob ConcurrentJob) {
	if newJob != nil {
		j.nextJob = make(chan ConcurrentJob, 1)
		j.nextJob <- newJob
	}
	close(j.nextJob)
	return
}

func (j *BaseConcurrentJob) GetNextJob() ConcurrentJob {
	if j.nextJob == nil {
		return nil
	}
	select {
	case job := <-j.nextJob:
		return job
	default:
		return nil
	}
}
