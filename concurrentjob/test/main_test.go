package test

import (
	"os"
	"testing"

	"gitlab.com/go-jor/jor-kit/concurrentjob"
)

var jobsBroker = concurrentjob.NewModelJobsBroker(50)

func TestMain(m *testing.M) {
	go jobsBroker.Start()
	exitCode := m.Run()
	os.Exit(exitCode)
}
