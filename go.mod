module gitlab.com/go-jor/jor-kit

go 1.16

require (
	cloud.google.com/go/storage v1.14.0
	github.com/360EntSecGroup-Skylar/excelize/v2 v2.4.0
	github.com/SebastiaanKlippert/go-wkhtmltopdf v1.6.1
	github.com/Shopify/sarama v1.28.0
	github.com/ajg/form v1.5.1 // indirect
	github.com/aliyun/aliyun-oss-go-sdk v2.2.2+incompatible
	github.com/baiyubin/aliyun-sts-go-sdk v0.0.0-20180326062324-cfa1a18b161f // indirect
	github.com/go-resty/resty/v2 v2.6.0
	github.com/go-sql-driver/mysql v1.8.0 // indirect
	github.com/golang/protobuf v1.5.2
	github.com/google/go-querystring v1.0.0
	github.com/imkira/go-interpol v1.1.0 // indirect
	github.com/kataras/iris/v12 v12.1.8
	github.com/moul/http2curl v1.0.0 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
	github.com/sergi/go-diff v1.1.0 // indirect
	github.com/shurcooL/sanitized_anchor_name v1.0.0 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/spf13/cobra v0.0.5
	github.com/spf13/pflag v1.0.3
	github.com/streadway/amqp v1.0.0
	github.com/valyala/fasthttp v1.22.0 // indirect
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c
	github.com/xeipuuv/gojsonschema v1.2.0 // indirect
	github.com/yalp/jsonpath v0.0.0-20180802001716-5cc68e5049a0 // indirect
	github.com/yudai/gojsondiff v1.0.0 // indirect
	github.com/yudai/golcs v0.0.0-20170316035057-ecda9a501e82 // indirect
	gitlab.com/go-emat/pdfcpu-mattex v1.0.25
	golang.org/x/net v0.0.0-20210428140749-89ef3d95e781
	golang.org/x/oauth2 v0.0.0-20210311163135-5366d9dc1934
	google.golang.org/grpc v1.45.0
	google.golang.org/protobuf v1.27.1
	gorm.io/driver/mysql v1.5.6
	gorm.io/gorm v1.25.8
)
