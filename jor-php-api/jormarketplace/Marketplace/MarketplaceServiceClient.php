<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Marketplace;

/**
 */
class MarketplaceServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Marketplace\UpdateTrackingRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function UpdateTracking(\Marketplace\UpdateTrackingRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/marketplace.MarketplaceService/UpdateTracking',
        $argument,
        ['\Jorcommon\SimpleResponse', 'decode'],
        $metadata, $options);
    }

}
