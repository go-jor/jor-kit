<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Wms;

/**
 */
class DeliveryNoteServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Wms\ReadDeliveryNoteRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function ReadDeliveryNote(\Wms\ReadDeliveryNoteRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/wms.DeliveryNoteService/ReadDeliveryNote',
        $argument,
        ['\Wms\ReadDeliveryNoteResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Wms\PreloadDeliveryNotesRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\ServerStreamingCall
     */
    public function PreloadDeliveryNotes(\Wms\PreloadDeliveryNotesRequest $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/wms.DeliveryNoteService/PreloadDeliveryNotes',
        $argument,
        ['\Wms\PreloadDeliveryNotesResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Wms\CreateDeliveryNoteRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function CreateDeliveryNote(\Wms\CreateDeliveryNoteRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/wms.DeliveryNoteService/CreateDeliveryNote',
        $argument,
        ['\Wms\CreateDeliveryNoteResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Wms\CancelDeliveryNoteRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function CancelDeliveryNote(\Wms\CancelDeliveryNoteRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/wms.DeliveryNoteService/CancelDeliveryNote',
        $argument,
        ['\Wms\CancelDeliveryNoteResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Wms\GetDeliveryRelateInventoryRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function GetDeliveryRelateInventory(\Wms\GetDeliveryRelateInventoryRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/wms.DeliveryNoteService/GetDeliveryRelateInventory',
        $argument,
        ['\Wms\GetDeliveryRelateInventoryResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Wms\VerifyCreateDeliveryNoteRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function VerifyCreateDeliveryNote(\Wms\VerifyCreateDeliveryNoteRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/wms.DeliveryNoteService/VerifyCreateDeliveryNote',
        $argument,
        ['\Wms\VerifyCreateDeliveryNoteResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Wms\ReadCarrierServiceRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function ReadCarrierService(\Wms\ReadCarrierServiceRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/wms.DeliveryNoteService/ReadCarrierService',
        $argument,
        ['\Wms\ReadCarrierServiceResponse', 'decode'],
        $metadata, $options);
    }

}
