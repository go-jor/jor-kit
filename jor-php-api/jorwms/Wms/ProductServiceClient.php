<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Wms;

/**
 */
class ProductServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Wms\ReadProductRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function ReadProduct(\Wms\ReadProductRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/wms.ProductService/ReadProduct',
        $argument,
        ['\Wms\ReadProductResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Wms\PreloadProductsRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\ServerStreamingCall
     */
    public function PreloadProducts(\Wms\PreloadProductsRequest $argument,
      $metadata = [], $options = []) {
        return $this->_serverStreamRequest('/wms.ProductService/PreloadProducts',
        $argument,
        ['\Wms\PreloadProductsResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Wms\ReadProductInventoryRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function ReadProductInventory(\Wms\ReadProductInventoryRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/wms.ProductService/ReadProductInventory',
        $argument,
        ['\Wms\ReadProductInventoryResponse', 'decode'],
        $metadata, $options);
    }

}
