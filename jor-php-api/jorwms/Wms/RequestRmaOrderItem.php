<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: wms/rma_order.proto

namespace Wms;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>wms.RequestRmaOrderItem</code>
 */
class RequestRmaOrderItem extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int64 product_id = 3;</code>
     */
    protected $product_id = 0;
    /**
     * Generated from protobuf field <code>string product_sku = 1;</code>
     */
    protected $product_sku = '';
    /**
     * Generated from protobuf field <code>int32 rma_qty = 2;</code>
     */
    protected $rma_qty = 0;
    /**
     * Generated from protobuf field <code>int64 delivery_note_item_id = 4;</code>
     */
    protected $delivery_note_item_id = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int|string $product_id
     *     @type string $product_sku
     *     @type int $rma_qty
     *     @type int|string $delivery_note_item_id
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Wms\RmaOrder::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int64 product_id = 3;</code>
     * @return int|string
     */
    public function getProductId()
    {
        return $this->product_id;
    }

    /**
     * Generated from protobuf field <code>int64 product_id = 3;</code>
     * @param int|string $var
     * @return $this
     */
    public function setProductId($var)
    {
        GPBUtil::checkInt64($var);
        $this->product_id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string product_sku = 1;</code>
     * @return string
     */
    public function getProductSku()
    {
        return $this->product_sku;
    }

    /**
     * Generated from protobuf field <code>string product_sku = 1;</code>
     * @param string $var
     * @return $this
     */
    public function setProductSku($var)
    {
        GPBUtil::checkString($var, True);
        $this->product_sku = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 rma_qty = 2;</code>
     * @return int
     */
    public function getRmaQty()
    {
        return $this->rma_qty;
    }

    /**
     * Generated from protobuf field <code>int32 rma_qty = 2;</code>
     * @param int $var
     * @return $this
     */
    public function setRmaQty($var)
    {
        GPBUtil::checkInt32($var);
        $this->rma_qty = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 delivery_note_item_id = 4;</code>
     * @return int|string
     */
    public function getDeliveryNoteItemId()
    {
        return $this->delivery_note_item_id;
    }

    /**
     * Generated from protobuf field <code>int64 delivery_note_item_id = 4;</code>
     * @param int|string $var
     * @return $this
     */
    public function setDeliveryNoteItemId($var)
    {
        GPBUtil::checkInt64($var);
        $this->delivery_note_item_id = $var;

        return $this;
    }

}

