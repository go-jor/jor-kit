<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Wms;

/**
 */
class RmaOrderServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Wms\CreateRmaOrderRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function CreateRmaOrder(\Wms\CreateRmaOrderRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/wms.RmaOrderService/CreateRmaOrder',
        $argument,
        ['\Wms\CreateRmaOrderResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Wms\VerifyCreateRmaOrderRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     * @return \Grpc\UnaryCall
     */
    public function VerifyCreateRmaOrder(\Wms\VerifyCreateRmaOrderRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/wms.RmaOrderService/VerifyCreateRmaOrder',
        $argument,
        ['\Wms\VerifyCreateRmaOrderResponse', 'decode'],
        $metadata, $options);
    }

}
