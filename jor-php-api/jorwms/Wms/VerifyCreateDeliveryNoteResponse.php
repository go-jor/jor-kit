<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: wms/delivery_note.proto

namespace Wms;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>wms.VerifyCreateDeliveryNoteResponse</code>
 */
class VerifyCreateDeliveryNoteResponse extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>bool success = 1;</code>
     */
    protected $success = false;
    /**
     * Generated from protobuf field <code>.wms.VerifyExistDeliveryNote exist_delivery_note = 2;</code>
     */
    protected $exist_delivery_note = null;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type bool $success
     *     @type \Wms\VerifyExistDeliveryNote $exist_delivery_note
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Wms\DeliveryNote::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>bool success = 1;</code>
     * @return bool
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * Generated from protobuf field <code>bool success = 1;</code>
     * @param bool $var
     * @return $this
     */
    public function setSuccess($var)
    {
        GPBUtil::checkBool($var);
        $this->success = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.wms.VerifyExistDeliveryNote exist_delivery_note = 2;</code>
     * @return \Wms\VerifyExistDeliveryNote|null
     */
    public function getExistDeliveryNote()
    {
        return $this->exist_delivery_note;
    }

    public function hasExistDeliveryNote()
    {
        return isset($this->exist_delivery_note);
    }

    public function clearExistDeliveryNote()
    {
        unset($this->exist_delivery_note);
    }

    /**
     * Generated from protobuf field <code>.wms.VerifyExistDeliveryNote exist_delivery_note = 2;</code>
     * @param \Wms\VerifyExistDeliveryNote $var
     * @return $this
     */
    public function setExistDeliveryNote($var)
    {
        GPBUtil::checkMessage($var, \Wms\VerifyExistDeliveryNote::class);
        $this->exist_delivery_note = $var;

        return $this;
    }

}

