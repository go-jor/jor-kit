<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: wms/rma_order.proto

namespace Wms;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>wms.VerifyCreateRmaOrderRequest</code>
 */
class VerifyCreateRmaOrderRequest extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>string reference_no = 1;</code>
     */
    protected $reference_no = '';
    /**
     * Generated from protobuf field <code>int64 oms_company_id = 2;</code>
     */
    protected $oms_company_id = 0;
    /**
     * Generated from protobuf field <code>string wms_merchant_code = 3;</code>
     */
    protected $wms_merchant_code = '';
    /**
     * Generated from protobuf field <code>string return_warehouse_code = 4;</code>
     */
    protected $return_warehouse_code = '';
    /**
     * Generated from protobuf field <code>string return_virtual_warehouse_code = 5;</code>
     */
    protected $return_virtual_warehouse_code = '';
    /**
     * Generated from protobuf field <code>int32 return_order_type = 6;</code>
     */
    protected $return_order_type = 0;
    /**
     * Generated from protobuf field <code>string return_tracking_no = 7;</code>
     */
    protected $return_tracking_no = '';
    /**
     * Generated from protobuf field <code>repeated .wms.RequestRmaOrderItem request_rma_items = 8;</code>
     */
    private $request_rma_items;
    /**
     * Generated from protobuf field <code>.jorcommon.Language lang = 10;</code>
     */
    protected $lang = 0;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type string $reference_no
     *     @type int|string $oms_company_id
     *     @type string $wms_merchant_code
     *     @type string $return_warehouse_code
     *     @type string $return_virtual_warehouse_code
     *     @type int $return_order_type
     *     @type string $return_tracking_no
     *     @type array<\Wms\RequestRmaOrderItem>|\Google\Protobuf\Internal\RepeatedField $request_rma_items
     *     @type int $lang
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\Wms\RmaOrder::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>string reference_no = 1;</code>
     * @return string
     */
    public function getReferenceNo()
    {
        return $this->reference_no;
    }

    /**
     * Generated from protobuf field <code>string reference_no = 1;</code>
     * @param string $var
     * @return $this
     */
    public function setReferenceNo($var)
    {
        GPBUtil::checkString($var, True);
        $this->reference_no = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int64 oms_company_id = 2;</code>
     * @return int|string
     */
    public function getOmsCompanyId()
    {
        return $this->oms_company_id;
    }

    /**
     * Generated from protobuf field <code>int64 oms_company_id = 2;</code>
     * @param int|string $var
     * @return $this
     */
    public function setOmsCompanyId($var)
    {
        GPBUtil::checkInt64($var);
        $this->oms_company_id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string wms_merchant_code = 3;</code>
     * @return string
     */
    public function getWmsMerchantCode()
    {
        return $this->wms_merchant_code;
    }

    /**
     * Generated from protobuf field <code>string wms_merchant_code = 3;</code>
     * @param string $var
     * @return $this
     */
    public function setWmsMerchantCode($var)
    {
        GPBUtil::checkString($var, True);
        $this->wms_merchant_code = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string return_warehouse_code = 4;</code>
     * @return string
     */
    public function getReturnWarehouseCode()
    {
        return $this->return_warehouse_code;
    }

    /**
     * Generated from protobuf field <code>string return_warehouse_code = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setReturnWarehouseCode($var)
    {
        GPBUtil::checkString($var, True);
        $this->return_warehouse_code = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string return_virtual_warehouse_code = 5;</code>
     * @return string
     */
    public function getReturnVirtualWarehouseCode()
    {
        return $this->return_virtual_warehouse_code;
    }

    /**
     * Generated from protobuf field <code>string return_virtual_warehouse_code = 5;</code>
     * @param string $var
     * @return $this
     */
    public function setReturnVirtualWarehouseCode($var)
    {
        GPBUtil::checkString($var, True);
        $this->return_virtual_warehouse_code = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 return_order_type = 6;</code>
     * @return int
     */
    public function getReturnOrderType()
    {
        return $this->return_order_type;
    }

    /**
     * Generated from protobuf field <code>int32 return_order_type = 6;</code>
     * @param int $var
     * @return $this
     */
    public function setReturnOrderType($var)
    {
        GPBUtil::checkInt32($var);
        $this->return_order_type = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string return_tracking_no = 7;</code>
     * @return string
     */
    public function getReturnTrackingNo()
    {
        return $this->return_tracking_no;
    }

    /**
     * Generated from protobuf field <code>string return_tracking_no = 7;</code>
     * @param string $var
     * @return $this
     */
    public function setReturnTrackingNo($var)
    {
        GPBUtil::checkString($var, True);
        $this->return_tracking_no = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated .wms.RequestRmaOrderItem request_rma_items = 8;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getRequestRmaItems()
    {
        return $this->request_rma_items;
    }

    /**
     * Generated from protobuf field <code>repeated .wms.RequestRmaOrderItem request_rma_items = 8;</code>
     * @param array<\Wms\RequestRmaOrderItem>|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setRequestRmaItems($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Wms\RequestRmaOrderItem::class);
        $this->request_rma_items = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>.jorcommon.Language lang = 10;</code>
     * @return int
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Generated from protobuf field <code>.jorcommon.Language lang = 10;</code>
     * @param int $var
     * @return $this
     */
    public function setLang($var)
    {
        GPBUtil::checkEnum($var, \Jorcommon\Language::class);
        $this->lang = $var;

        return $this;
    }

}

