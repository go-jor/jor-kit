package kafka

import (
	"fmt"
	"github.com/Shopify/sarama"
	"log"
	"os"
	"os/signal"
	"regexp"
	"syscall"
	"testing"
	"time"
)

const TestConsumerUser = "test-operation-user"
const TestConsumerUserPassword = "test-operation-secret"
const TestConsumerUserGroup = "test-document-group"
const TestConsumerTopic = "test-document"

func initSubscriber(t *testing.T) (*Subscriber, error) {
	cfg := &ConsumerConfig{
		Brokers:       []string{"localhost:9092", "localhost:9093", "localhost:9094"},
		Topics:        []string{TestConsumerTopic},
		MaxRetry:      5,
		GroupID:       TestConsumerUserGroup,
		User:          TestConsumerUser,
		Password:      TestConsumerUserPassword,
		SASLEnable512: true,
		Version:       "2.1.1",
		OffsetOldest:   true,
		Assignor:      sarama.BalanceStrategyRange,
	}
	handler := &tmpMsgHandler{log: t}
	return NewSubscriber(cfg, handler)
}

type tmpMsgHandler struct {
	log *testing.T
}

func (t *tmpMsgHandler) Consume(msg *ConsumerMessage) error {
	logMsg := fmt.Sprintf("Receive topic[%s]-partition[%d]-offset[%d] message (key=%s, value=[%s]",
		msg.Topic, msg.Partition, msg.Offset, string(msg.Key), string(msg.Value))

	// t.log.Log(logMsg)
	log.Println(logMsg)

	// control consume speed
	time.Sleep(time.Second)

	return nil
}

func TestEmatConsumer(t *testing.T) {
	log.SetFlags(log.Ldate | log.Lshortfile)
	sarama.Logger = log.New(os.Stdout, "[sarama] ", log.LstdFlags)
	subscriber, err := initSubscriber(t)
	if err != nil {
		t.Error(err)
	}
	defer func() {
		subscriber.Close()
	}()
	// Create signal channel
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM)

	go subscriber.Open()

	<-sigchan
	log.Println("Receive signal, exited")
}

func TestRegexp(t *testing.T) {

	availableTopics := []string{"1-supplier-marketplace", "2-supplier-marketplace", "10-supplier-document"}
	expr := `.*-supplier-marketplace`
	reg, err := regexp.Compile(expr)
	log.Println(err)
	for _, t := range availableTopics {
		log.Println(reg.MatchString(t))
	}
}