package kafka

import (
	"context"
	"errors"
	"fmt"
	"log"
	"time"
)

type Event struct {
	Type string //for consumer to consume message
}

type ProducerEvent struct {
	Event
	ProduceKafkaMessageID int64
	Topic                 string
	Key                   string
	Producer              *EmatProducer
	Message               []byte
	agtCtx                context.Context
	stopChan              chan struct{}
}

func (e *ProducerEvent) Process(agtCtx context.Context) *EventResult {
	var err error
	fmt.Println("Start event", e.Type)
	if e.Producer == nil {
		return &EventResult{
			ProduceKafkaMessageID: e.ProduceKafkaMessageID,
			EventError:            errors.New("kafka MQ error: no found producer"),
		}
	}
	if e.Producer.syncProducer == nil {
		fmt.Println("Event reconnect producer: ", e.Producer.cfg)
		e.Producer, err = NewEmatProducer(e.Producer.cfg)
		if err != nil {
			return &EventResult{
				ProduceKafkaMessageID: e.ProduceKafkaMessageID,
				EventError:            err,
			}
		}
	}
	// normal flow
	for {
		select {
		case <-agtCtx.Done():
			e.stopChan <- struct{}{}
			break
		default:
			time.Sleep(time.Millisecond * 50)

			partition, offset, err := e.Producer.SendMessage(e.Topic, e.Key, e.Message)
			log.Println(partition, offset, err)
			return &EventResult{
				ProduceKafkaMessageID: e.ProduceKafkaMessageID,
				Partition:             partition,
				Offset:                offset,
				EventError:            err,
			}
		}
	}
}

func (e *ProducerEvent) Stop() error {
	fmt.Println("Stop Event Key", e.Key)
	select {
	case <-e.stopChan:
		return nil
	case <-time.After(time.Second * 1):
		return errors.New("failed to stop event for timeout")
	}
}
