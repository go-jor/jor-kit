package migrate

import (
	_ "gorm.io/driver/mysql"
	"gorm.io/gorm"
	"time"
)

type BaseModel struct {
	CreatedAt time.Time      `json:"created_at" gorm:"type:timestamp not null;default:now() "`
	UpdatedAt time.Time      `json:"updated_at" gorm:"type:timestamp not null;default:now() "`
	DeletedAt gorm.DeletedAt `gorm:"type:timestamp null;"`
	CreatedBy int64          `json:"created_by" gorm:"not null;default:0 "`
	UpdatedBy int64          `json:"updated_by" gorm:"not null;default:0 "`
}

type Migration struct {
	ID        int64  `gorm:"primary_key" json:"id"`
	Migration string `json:"migration"`
	Batch     int    `json:"batch"`
}

type Result struct {
	MaxBatch int
}

func (m *Migration) PrepareDatabase(migrator *Migrator) {
	migration := &Migration{}
	found := migrator.DB.Migrator().HasTable(migration)
	if !found {
		migrator.DB.Migrator().CreateTable(migration)
	}
}

func (m *Migration) UpMigrate(migrator *Migrator, migrationFile string, nextBatchNumber int) {
	migration := &Migration{
		Migration: migrationFile,
		Batch:     nextBatchNumber,
	}
	migrator.DB.Where("migration = ?", migration.Migration).First(migration)
	if migration.ID <= 0 {
		if err := migrator.DB.Create(migration).Error; err != nil {
			panic(err.Error())
		}
	}
}

func (m *Migration) DownMigrate(migrator *Migrator, migrationFile string, maxBatchNumber int) {
	migrator.DB.Where("migration =? AND batch >= ?", migrationFile, maxBatchNumber).Delete(Migration{})
}

func (m *Migration) GetRan(migrator *Migrator) (migrations []string) {
	migrator.DB.Model(&Migration{}).Order("batch asc").Order("migration asc").Pluck("migration", &migrations)
	return
}

func (m *Migration) GetRollBackRan(migrator *Migrator, maxBatchNumber int) (migrations []string) {
	migrator.DB.Model(&Migration{}).Where("batch =?", maxBatchNumber).Order("migration desc").Pluck("migration", &migrations)
	return
}

func (m *Migration) getNextBatchNumber(migrator *Migrator) int {
	maxBatchNumber := m.getMaxBatchNumber(migrator)
	return maxBatchNumber + 1
}

func (m *Migration) getMaxBatchNumber(migrator *Migrator) int {
	var result Result
	migrator.DB.Raw("SELECT max(batch) as max_batch FROM migrations").Scan(&result)
	return result.MaxBatch
}
