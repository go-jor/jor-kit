package queue

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"reflect"
	"time"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	_ "github.com/go-sql-driver/mysql"
)

// database config
var (
	JobTable     = "jobs"
	DefaultQueue = "default"
	RetryAfter   = 90
)

// DatabaseQueue struct
type DatabaseQueue struct {
	db *gorm.DB
}

// NewDatabaseQueue  return database queue
func NewDatabaseQueue(c *Configuration) (*DatabaseQueue, error) {
	sqlConnection := c.DBUserName + ":" + c.DBPassword + "@tcp(" + c.DBHost + ":" + c.DBPort + ")/" + c.DatabaseName + "?charset=utf8&parseTime=True&loc=Local"
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second,   // Slow SQL threshold
			LogLevel:                  logger.Silent, // Log level
			IgnoreRecordNotFoundError: true,          // Ignore ErrRecordNotFound error for logger
			//Colorful:                  false,         // Disable color
		},
	)
	dbConnection, err := gorm.Open(mysql.New(mysql.Config{
		DSN:                       sqlConnection, // data source name
		DefaultStringSize:         256,           // default size for string fields
		DisableDatetimePrecision:  true,          // disable datetime precision, which not supported before MySQL 5.6
		DontSupportRenameIndex:    true,          // drop & create when rename index, rename index not supported before MySQL 5.7, MariaDB
		DontSupportRenameColumn:   true,          // `change` when rename column, rename column not supported before MySQL 8, MariaDB
		SkipInitializeWithVersion: false,         // auto configure based on currently MySQL version
	}), &gorm.Config{
		Logger: newLogger,
	})
	if err != nil {
		panic(err.Error())
	}
	if c.MaxLifeTime > 0 {
		connectDb, err := dbConnection.DB()
		if err != nil {
			return nil, err
		}
		connectDb.SetConnMaxLifetime(time.Second * time.Duration(c.MaxLifeTime))
	}
	queue := &DatabaseQueue{
		db: dbConnection,
	}
	return queue, nil
}

// Push a job into the queue
func (q *DatabaseQueue) Push(job interface{}, data interface{}, queue string, delaySecond int64) error {
	return q.pushToDatabase(queue, q.CreatePayload(job, data), delaySecond, 0)
}

// Pop the next job off of the queue.
func (q *DatabaseQueue) Pop(queue string) IJob {
	queue = q.getQueue(queue)
	if job := q.getNextAvailableJob(queue); job != nil {
		q.markJobAsReserved(job)
		return job
	}
	return nil
}

// Retry all
func (q *DatabaseQueue) Retry() {
	failedJobs := q.getAllFailedJob()
	for _, failedJob := range failedJobs {
		q.retryJob(failedJob)
	}
}

// RetryByID retry a job
func (q *DatabaseQueue) RetryByID(id int64) {
	failedJob, found := q.getFailedJob(id)
	if found {
		q.retryJob(failedJob)
	} else {
		fmt.Printf("Unable to find failed job with ID [%v].\n", id)
	}
}

// Release a reserved job back onto the queue.
func (q *DatabaseQueue) Release(queue string, job IJob, delay int64) error {
	return q.pushToDatabase(queue, job.GetRecord().Payload, delay, job.Attempts())
}

// CreatePayload return payload string
func (q *DatabaseQueue) CreatePayload(job interface{}, data interface{}) string {
	JobString, _ := json.Marshal(job)
	payload := &Payload{
		CommandName: reflect.TypeOf(job).Elem().Name(),
		Command:     string(JobString),
	}
	// payloadString := []byte{}
	payloadString, _ := json.Marshal(payload)

	return string(payloadString)
}

func (q *DatabaseQueue) pushToDatabase(queue string, payload string, delay int64, attempts int64) error {
	record := q.buildDatabaseRecord(q.getQueue(queue), payload, q.availableAt(delay), attempts)
	if err := q.db.Create(&record).Error; err != nil {
		return err
	}
	return nil
}

func (q *DatabaseQueue) LogFailedJob(job *Jobs, err error) FailedJobs {
	failedJob := FailedJobs{
		Connection: "database",
		Queue:      job.Queue,
		Payload:    job.Payload,
		Exception:  err.Error(),
		FailedAt:   time.Now(),
	}
	if err := q.db.Create(&failedJob).Error; err != nil {
		return FailedJobs{}
	}
	return failedJob
}

func (q *DatabaseQueue) getAllFailedJob() []FailedJobs {
	failedJobs := []FailedJobs{}
	q.db.Model(&FailedJobs{}).Find(&failedJobs)
	return failedJobs
}

func (q *DatabaseQueue) getFailedJob(id int64) (failedJob FailedJobs, found bool) {
	if err := q.db.Find(&failedJob, id).Error; err == nil {
		found = true
	}
	return
}

func (q *DatabaseQueue) retryJob(failedJob FailedJobs) error {
	err := q.pushToDatabase(failedJob.Queue, failedJob.Payload, 0, 0)
	if err != nil {
		return err
	} else {
		fmt.Printf("The failed job [%v] has been pushed back onto the queue!\n", failedJob.ID)
		return q.forgetFailed(failedJob)
	}
}

func (q *DatabaseQueue) forgetFailed(failedJob FailedJobs) error {
	return q.db.Delete(&failedJob).Error
}

func (q *DatabaseQueue) availableAt(delay int64) time.Time {
	return time.Now().Add(time.Duration(delay) * time.Second)
}

func (q *DatabaseQueue) getQueue(queue string) string {
	if queue != "" {
		return queue
	}
	return DefaultQueue
}

func (q *DatabaseQueue) buildDatabaseRecord(queue string, payload string, availableAt time.Time, attempts int64) Jobs {
	return Jobs{
		Queue:       queue,
		Attempts:    attempts,
		AvailableAt: availableAt,
		CreatedAt:   time.Now(),
		Payload:     payload,
	}
}

func (q *DatabaseQueue) Size(queue string) int64 {
	var count int64
	queue = q.getQueue(queue)
	if queue != "all" {
		q.db.Model(&Jobs{}).Where("queue = ?", queue).Count(&count)
	} else {
		q.db.Model(&Jobs{}).Count(&count)
	}
	return count
}

func (q *DatabaseQueue) getNextAvailableJob(queue string) IJob {
	job := Jobs{}
	db := q.db.Where("((reserved_at is null and available_at <= ?) or reserved_at <= ? )", time.Now(), q.getReservedTime())
	if queue != "all" {
		db = db.Where("queue = ?", queue)
	}
	err := db.Order("id").First(&job).Error
	if err == nil {
		return DatabaseJobRecord{Record: &job, Database: q}
	}
	return nil
}

func (q *DatabaseQueue) getReservedTime() time.Time {
	return time.Now().Add(time.Duration(-RetryAfter) * time.Second)
}

func (q *DatabaseQueue) markJobAsReserved(job IJob) error {
	job.Touch()
	job.Increment()

	err := q.db.Save(job.GetRecord()).Error
	return err
}

func (q *DatabaseQueue) DeleteReserved(record *Jobs) bool {
	if err := q.db.Delete(record).Error; err == nil {
		return true
	}
	return false
}

func (q *DatabaseQueue) Close() {
	if q.db != nil {
		connectionDb, err := q.db.DB()
		if err != nil {
			fmt.Printf("Database Get connect DB failed: %v", err)
		}
		if err := connectionDb.Close(); err != nil {
			fmt.Printf("Database Queue close db connect failed: %v", err)
		}
		fmt.Println("Database Queue db connect closed")
	}
}
