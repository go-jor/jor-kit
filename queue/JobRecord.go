package queue

import (
	"encoding/json"
	"fmt"
	"log"
	"time"
)

type Jobs struct {
	ID          int64
	Queue       string
	Attempts    int64
	ReservedAt  *time.Time
	AvailableAt time.Time
	CreatedAt   time.Time
	Payload     string
}

type FailedJobs struct {
	ID         int64
	Connection string
	Queue      string
	Payload    string
	Exception  string
	FailedAt   time.Time
}

type Payload struct {
	CommandName string
	Command     string
}

type IJob interface {
	GetJobId() int64
	GetName() string
	Touch()
	Increment()
	GetRecord() *Jobs
	Attempts() int64
	Fire(ShouldQueue)
	Failed(ShouldQueue, error)
	Payload() Payload
	Delete()
	// Release(delay int64)
}

type DatabaseJobRecord struct {
	Record   *Jobs
	Database *DatabaseQueue
}

func (j DatabaseJobRecord) GetJobId() int64 {
	return j.Record.ID
}

func (j DatabaseJobRecord) GetName() string {
	return j.Payload().CommandName
}

func (j DatabaseJobRecord) Touch() {
	now := time.Now()
	j.Record.ReservedAt = &now
}

func (j DatabaseJobRecord) Increment() {
	j.Record.Attempts = j.Record.Attempts + 1
}

func (j DatabaseJobRecord) GetRecord() *Jobs {
	return j.Record
}

func (j DatabaseJobRecord) Attempts() int64 {
	return j.Record.Attempts
}

func (j DatabaseJobRecord) Payload() Payload {
	payload := Payload{}
	json.Unmarshal([]byte(j.GetRecord().Payload), &payload)
	return payload
}

func (j DatabaseJobRecord) Fire(shouldQueue ShouldQueue) {
	err := shouldQueue.Handle(j.Payload().Command)
	if err == nil {
		j.Delete()
	} else {
		log.Printf("%+v", err)
		log.Printf("[%v] [%v] %d %v %v \n", time.Now().Format("2006-01-02 00:00:00"), j.GetJobId(), j.Attempts(), " attempt failed", j.GetName())
		//if have err, do not deal it wait next time (RetryAfter = 90) or push to fail job at once.
		// j.Failed(shouldQueue, err) //push to fail job at once
	}
}

func (j DatabaseJobRecord) Delete() {
	j.Database.DeleteReserved(j.GetRecord())
}

func (j DatabaseJobRecord) Failed(shouldQueue ShouldQueue, err error) {
	// may process when the job failed eg: shouldQueue.Failed()
	jobFailedFuncErr := shouldQueue.Failed(j.Payload().Command)
	if jobFailedFuncErr != nil {
		log.Printf("[%v] [%v] %d %v %v \n", time.Now().Format("2006-01-02 00:00:00"), j.GetJobId(), j.Attempts(), " Failed process of job failed", j.GetName())
	}

	fmt.Printf("[%v] [%v] %v %v \n", time.Now().Format("2006-01-02 00:00:00"), j.GetJobId(), "failed", j.GetName())
	j.Delete()
	//push job to failed_job
	j.Database.LogFailedJob(j.GetRecord(), err)
}
