package queue

import "time"

//MailRecords struct
type MailRecords struct {
	ID         int64 `gorm:"primary_key" json:"id"`
	To         string
	Subject    string
	Body       string
	Attachment string
	Attempts   int64
	Status     string
	Exception  string
	CreatedAt  time.Time
	UpdatedAt  time.Time
}
