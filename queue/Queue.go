package queue

import (
	"github.com/pkg/errors"
)

//Configuration queue config
type Configuration struct {
	Driver    string
	Table     string
	QueueName string

	//database config
	DatabaseName string
	DBUserName   string
	DBPassword   string
	DBPort       string
	DBHost       string
	MaxLifeTime  int
}

//Queue struct
type Queue struct {
	Configuration *Configuration
	Worker        *Worker
	QueueManager  IQueue
}

//IQueue interface
type IQueue interface {
	Push(job interface{}, data interface{}, queue string, delaySecond int64) error
	Pop(queue string) IJob
	Release(queue string, job IJob, delay int64) error
	Retry()
	RetryByID(id int64)
	Close()
	// LaterOn(queue string, delay int64, job string, data interface{}) interface{}
	// Later(delay int64, job string, data interface{}, queue string) interface{}
	// Bulk(jobs []string, data interface{}, queue string)
}

//NewQueue return queue
func NewQueue(c *Configuration) (*Queue, error) {
	if c.Driver == "" {
		return nil, errors.New("Please Use Driver (database...)")
	}

	var iqueue IQueue
	dbQueue, err := NewDatabaseQueue(c)
	if err != nil {
		return nil, err
	}
	iqueue = dbQueue
	_, ok := iqueue.(IQueue)
	if !ok {
		return nil, errors.New("new a queue drive failed. ")
	}

	queue := &Queue{
		Configuration: c,
		Worker:        &Worker{QueueManager: iqueue},
		QueueManager:  iqueue,
	}

	return queue, nil
}

func (q *Queue) isShouldQueueType(command interface{}) error {
	//判断command是否实现了 ShouldQueue接口
	_, ok := command.(ShouldQueue)
	if !ok {
		return errors.New("the job is not implement ShouldQueue. ")
	}
	return nil
}

//Dispatch push
func (q *Queue) Dispatch(command interface{}, queueName string) error {
	err := q.isShouldQueueType(command)
	if err != nil {
		return err
	}

	return q.pushCommandToQueue(q.QueueManager, command, queueName, 0)
}

//Dispatch push with delay (in second)
func (q *Queue) DelayDispatch(command interface{}, queueName string, delaySecond int64) error {
	err := q.isShouldQueueType(command)
	if err != nil {
		return err
	}

	return q.pushCommandToQueue(q.QueueManager, command, queueName, delaySecond)
}

func (q *Queue) pushCommandToQueue(queue IQueue, command interface{}, queueName string, delaySecond int64) error {
	return queue.Push(command, "", queueName, delaySecond)
}

//Retry all
func (q *Queue) Retry() {
	q.QueueManager.Retry()
}

//RetryByID retry a job
func (q *Queue) RetryByID(id int64) {
	q.QueueManager.RetryByID(id)
}

func (q *Queue) Close() {
	q.QueueManager.Close()
}
