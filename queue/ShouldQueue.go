package queue

type ShouldQueue interface {
	Handle(dataJson string) error
	Failed(dataJson string) error
}
