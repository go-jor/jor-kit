package queue

import (
	"fmt"
	"time"

	"github.com/pkg/errors"
)

type Worker struct {
	QueueManager IQueue
}

// func (w *Worker) Daemon(job IJob, shouldQueue ShouldQueue) {
// 	//@todo 开启一个死循环
// 	w.Run(job, shouldQueue)
// }

func (w *Worker) GetNextJob(queue string) IJob {
	return w.QueueManager.Pop(queue)
}

func (w *Worker) Run(job IJob, shouldQueue ShouldQueue) {
	w.process(job, shouldQueue)
}

func (w *Worker) process(job IJob, shouldQueue ShouldQueue) error {
	fmt.Printf("[%v] [%v] %v %v \n", time.Now().Format("2006-01-02 00:00:00"), job.GetJobId(), "processing", job.GetName())

	err := w.markJobAsFailedIfAlreadyExceedsMaxAttempts(job, 3)
	if err != nil {
		job.Failed(shouldQueue, err)
	} else {
		job.Fire(shouldQueue)
		fmt.Printf("[%v] [%v] %v %v \n", time.Now().Format("2006-01-02 00:00:00"), job.GetJobId(), "success", job.GetName())
	}
	// may call job.Release() to retry queue job if not failed
	return nil
}

func (w *Worker) markJobAsFailedIfAlreadyExceedsMaxAttempts(job IJob, maxTries int64) error {
	if maxTries == 0 || job.Attempts() < maxTries {
		return nil
	}
	err := errors.New(job.GetName() + " has been attempted too many times.")
	return err
}
