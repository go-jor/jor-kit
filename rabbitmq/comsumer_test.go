package rabbitmq

import (
	"fmt"
	"testing"
	"time"
)

func TestConsumer(t *testing.T) {
	m, err := New(mqUrl).Open()
	if err != nil {
		panic(err.Error())
	}
	defer m.Close()

	consumer, err:= m.Consumer("test-consumer-2")
	if err!=nil{
		panic(fmt.Sprintf("Create consumer failed, %v", err))
	}
	defer consumer.Close()

	exchangeBinds := []*ExchangeBinds{
		&ExchangeBinds{
			Exchange: NewDefaultExchange("exch.unitest", ExchangeDirect),
			Bindings: []*Binding{
				&Binding{
					RouteKey: "route.unitest1",
					Queues: []*Queue{
						NewDefaultQueue("queue.unitest1"),
					},
				},
				&Binding{
					RouteKey: "route.unitest2",
					Queues: []*Queue{
						NewDefaultQueue("queue.unitest2"),
					},
				},
			},
		},
	}

	msgChan := make(chan Delivery, 1)

	defer close(msgChan)

	consumer.SetExchangeBinds(exchangeBinds)
	consumer.SetMsgCallback(msgChan)
	consumer.SetQos(10)

	if err = consumer.Open(); err != nil {
		panic(fmt.Sprintf("Open failed, %v", err))
	}

	i := 0
	for msg := range msgChan{
		i++
		if i%5==0 {
			consumer.CloseChan()
		}
		t.Logf("Consumer receive msg `%s`\n", string(msg.Body))
		time.Sleep(time.Second)
	}
}