package rabbitmq

import (
	"errors"
	"fmt"
	"github.com/streadway/amqp"
	"log"
	"sync"
	"time"
)

type Delivery struct {
	amqp.Delivery
}

type Consumer struct {
	// Consumer的名字, "" is OK
	name string

	// MQ实例
	rabbitMQ *RabbitMQ

	// 保护数据并发安全
	mutex sync.RWMutex

	// MQ的会话channel
	mqChannel *amqp.Channel

	// MQ的exchange与其绑定的queues
	exchangeBinds []*ExchangeBinds

	// Oos prefetch
	prefetch int

	// 上层用于接收消费出来的消息的管道
	callback chan<- Delivery

	// 监听会话channel关闭
	closeChan chan *amqp.Error
	// Consumer关闭控制
	stopChan chan struct{}

	// Consumer状态
	state uint8
}

func newConsumer(name string, rabbitMQ *RabbitMQ) *Consumer {
	return &Consumer{
		name:     name,
		rabbitMQ: rabbitMQ,
		stopChan: make(chan struct{}),
	}
}

func (c Consumer) Name() string {
	return c.name
}

// CloseChan 该接口仅用于测试使用, 勿手动调用
func (c *Consumer) CloseChan() {
	c.mutex.Lock()
	c.mqChannel.Close()
	c.mutex.Unlock()
}

func (c *Consumer) SetExchangeBinds(eb []*ExchangeBinds) *Consumer {
	c.mutex.Lock()
	if c.state != StateOpened {
		c.exchangeBinds = eb
	}
	c.mutex.Unlock()
	return c
}

func (c *Consumer) SetMsgCallback(cb chan<- Delivery) *Consumer {
	c.mutex.Lock()
	c.callback = cb
	c.mutex.Unlock()
	return c
}

// SetQos 设置channel粒度的Qos, prefetch取值范围[0,∞), 默认为0
// 如果想要RoundRobin地进行消费，设置prefetch为1即可
// 注意:在调用Open前设置
func (c *Consumer) SetQos(prefetch int) *Consumer {
	c.mutex.Lock()
	c.prefetch = prefetch
	c.mutex.Unlock()
	return c
}

func (c *Consumer) Open() error {
	// Open期间不允许对channel做任何操作
	c.mutex.Lock()
	defer c.mutex.Unlock()

	// 参数校验
	if c.rabbitMQ == nil {
		return errors.New("RabbitMQ: Bad consumer")
	}
	if len(c.exchangeBinds) <= 0 {
		return errors.New("RabbitMQ: No exchangeBinds found. You should SetExchangeBinds brefore open.")
	}

	// 状态检测
	if c.state == StateOpened {
		return errors.New("RabbitMQ: Consumer had been opened")
	}

	// 初始化channel
	connChannel, err := c.rabbitMQ.connChannel()
	if err != nil {
		return fmt.Errorf("RabbitMQ: Create channel failed, %v", err)
	}

	err = func(ch *amqp.Channel) error {
		var e error
		if e = applyExchangeBinds(ch, c.exchangeBinds); e != nil {
			return e
		}
		if e = ch.Qos(c.prefetch, 0, false); e != nil {
			return e
		}
		return nil
	}(connChannel)
	if err != nil {
		connChannel.Close()
		return fmt.Errorf("RabbitMQ: %v", err)
	}

	c.mqChannel = connChannel
	c.state = StateOpened
	c.stopChan = make(chan struct{})
	c.closeChan = make(chan *amqp.Error, 1)
	c.mqChannel.NotifyClose(c.closeChan)

	// 开始循环消费
	consumeOption := DefaultConsumeOption()
	notify := make(chan error, 1)
	c.consume(consumeOption, notify)
	for e := range notify {
		if e != nil {
			log.Printf("[ERROR] %v\n", e)
			continue
		}
		break
	}
	close(notify)

	// 健康检测
	go c.keepalive()

	return nil
}

func (c *Consumer) Close() {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	select {
	case <-c.stopChan:
		// had been closed
	default:
		close(c.stopChan)
	}
}

// notifyErr 向上层抛出错误, 如果error为空表示执行完成.由上层负责关闭channel
func (c *Consumer) consume(opt *ConsumeOption, notifyErr chan<- error) {
	for idx, eb := range c.exchangeBinds {
		if eb == nil {
			notifyErr <- fmt.Errorf("RabbitMQ: ExchangeBinds[%d] is nil, consumer(%s)", idx, c.name)
			continue
		}
		for i, b := range eb.Bindings {
			if b == nil {
				notifyErr <- fmt.Errorf("RabbitMQ: Binding[%d] is nil, ExchangeBinds[%d], consumer(%s)", i, idx, c.name)
				continue
			}
			for qi, q := range b.Queues {
				if q == nil {
					notifyErr <- fmt.Errorf("RabbitMQ: Queue[%d] is nil, ExchangeBinds[%d], Biding[%d], consumer(%s)", qi, idx, i, c.name)
					continue
				}
				delivery, err := c.mqChannel.Consume(q.Name, "", opt.AutoAck, opt.Exclusive, opt.NoLocal, opt.NoWait, opt.Args)
				if err != nil {
					notifyErr <- fmt.Errorf("RabbitMQ: Consumer(%s) consume queue(%s) failed, %v", c.name, q.Name, err)
					continue
				}
				go c.deliver(delivery)
			}
		}
	}
	notifyErr <- nil
}

func (c *Consumer) deliver(delivery <-chan amqp.Delivery) {
	for d := range delivery {
		if c.callback != nil {
			c.callback <- Delivery{d}
		}
	}
}

func (c *Consumer) State() uint8 {
	c.mutex.RLock()
	defer c.mutex.RUnlock()
	return c.state
}

func (c *Consumer) keepalive() {
	select {
	case <-c.stopChan:
		// 正常关闭
		log.Printf("[WARN] Consumer(%s) shutdown normally\n", c.Name())
		c.mutex.Lock()
		c.mqChannel.Close()
		c.mqChannel = nil
		c.state = StateClosed
		c.mutex.Unlock()

	case err := <-c.closeChan:
		if err == nil {
			log.Printf("[ERROR] RabbitMQ: Consumer(%s)'s channel was closed, but Error detail is nil\n", c.name)
		} else {
			log.Printf("[ERROR] RabbitMQ: Consumer(%s)'s channel was closed, code:%d, reason:%s\n", c.name, err.Code, err.Reason)
		}

		// channel被异常关闭了
		c.mutex.Lock()
		c.state = StateReopening
		c.mutex.Unlock()

		maxRetry := 99999999
		for i := 0; i < maxRetry; i++ {
			time.Sleep(time.Second)
			if c.rabbitMQ.State() != StateOpened {
				log.Printf("[WARN] RabbitMQ: Consumer(%s) try to recover channel for %d times, but mq's state != StateOpened\n", c.name, i+1)
				continue
			}
			if e := c.Open(); e != nil {
				if amqpErr, ok := e.(*amqp.Error); ok && amqpErr.Code == 501 {
					log.Fatalf("[ERROR] RabbitMQ: Consumer(%s) recover channel failed for %d times, Err:%v\n", c.name, i+1, amqpErr)
				}
				log.Printf("[WARN] RabbitMQ: Consumer(%s) recover channel failed for %d times, Err:%v\n", c.name, i+1, e)
				continue
			}
			log.Printf("[INFO] RabbitMQ: Consumer(%s) recover channel OK. Total try %d times\n", c.name, i+1)
			return
		}
		log.Printf("[ERROR] RabbitMQ: Consumer(%s) try to recover channel over maxRetry(%d), so exit\n", c.name, maxRetry)
	}
}
