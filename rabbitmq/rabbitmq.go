package rabbitmq

import (
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"github.com/streadway/amqp"
	"io/ioutil"
	"log"
	"os"
	"sync"
	"time"
)

var (
	StateClosed    = uint8(0)
	StateOpened    = uint8(1)
	StateReopening = uint8(2)
)

type RabbitMQ struct {
	// RabbitMQ连接的url
	url string

	// 保护内部数据并发读写
	mutex sync.RWMutex

	// RabbitMQ TCP连接
	conn *amqp.Connection

	producers []*Producer
	consumers []*Consumer

	// RabbitMQ 监听连接错误
	closeChan chan *amqp.Error
	// 监听用户手动关闭
	stopChan chan struct{}

	// RabbitMQ状态
	state uint8
}

func New(url string) *RabbitMQ {
	return &RabbitMQ{
		url:       url,
		producers: make([]*Producer, 0, 1),
		state:     StateClosed,
	}
}

func (m *RabbitMQ) Open() (mq *RabbitMQ, err error) {
	// 进行Open期间不允许做任何跟连接有关的事情
	m.mutex.Lock()
	defer m.mutex.Unlock()

	if m.state == StateOpened {
		return m, errors.New("RabbitMQ: Had been opened")
	}

	if m.conn, err = m.openRabbitMQConn(); err != nil {
		return m, fmt.Errorf("RabbitMQ: Dial err: %v", err)
	}

	m.state = StateOpened
	m.stopChan = make(chan struct{})
	m.closeChan = make(chan *amqp.Error, 1)
	m.conn.NotifyClose(m.closeChan)

	go m.keepalive()

	return m, nil
}

func (m *RabbitMQ) openRabbitMQConn() (conn *amqp.Connection, err error) {
	// 进行Open期间不允许做任何跟连接有关的事情
	serverName := os.Getenv("RABBIT_MQ_SERVER_NAME")
	if mtls := os.Getenv("RABBIT_MQ_MTLS"); mtls != "" {
		caCertCrt := "/etc/rabbitmq-certs/ca.crt"
		clientCertKey := "/etc/rabbitmq-certs/tls.key"
		clientCertCrt := "/etc/rabbitmq-certs/tls.crt"
		if os.Getenv("RABBIT_MQ_CA_CERT") != "" {
			caCertCrt = os.Getenv("RABBIT_MQ_CA_CERT")
		}
		if os.Getenv("RABBIT_MQ_CLIENT_CERT") != "" {
			clientCertCrt = os.Getenv("RABBIT_MQ_CLIENT_CERT")
		}
		if os.Getenv("RABBIT_MQ_CLIENT_KEY") != "" {
			clientCertKey = os.Getenv("RABBIT_MQ_CLIENT_KEY")
		}
		println(caCertCrt, clientCertCrt, clientCertKey)
		return m.openMutualTLS(caCertCrt, clientCertCrt, clientCertKey, serverName)
	}

	if tls := os.Getenv("RABBIT_MQ_TLS"); tls != "" {
		certFile := "/etc/rabbitmq-certs/tls.crt"
		if os.Getenv("RABBIT_MQ_SERVER_CERT") != "" {
			certFile = os.Getenv("RABBIT_MQ_SERVER_CERT")
		}
		return m.openTLS(certFile, serverName)
	}

	return  amqp.Dial(m.url)
}

func (m *RabbitMQ) openMutualTLS(caCertCrt, clientCertCrt, clientCertKey, serverNameOverride string) (conn *amqp.Connection, err error) {

	caCert, err := ioutil.ReadFile(caCertCrt)
	if err != nil {
		log.Fatalf("ioutil.ReadFile err: %v", err)
	}
	cert, err := tls.LoadX509KeyPair(clientCertCrt, clientCertKey)
	if err != nil {
		return nil, err
	}
	rootCAs := x509.NewCertPool()
	if ok := rootCAs.AppendCertsFromPEM(caCert); !ok {
		log.Fatalf("certPool.AppendCertsFromPEM err")
	}
	tlsConfig := &tls.Config{
		RootCAs:      rootCAs,
		Certificates: []tls.Certificate{cert},
		//InsecureSkipVerify: true,
	}
	if serverNameOverride != "" {
		tlsConfig.ServerName = serverNameOverride
	}
	return amqp.DialTLS(m.url, tlsConfig)
}

//
func (m *RabbitMQ) openTLS(certFile, serverNameOverride string) (conn *amqp.Connection, err error) {

	b, err := ioutil.ReadFile(certFile)
	if err != nil {
		return nil, err
	}
	cp := x509.NewCertPool()
	if !cp.AppendCertsFromPEM(b) {
		return nil, fmt.Errorf("credentials: failed to append certificates")
	}
	tlsConfig := &tls.Config{
		RootCAs:      cp,
		//InsecureSkipVerify: true,
	}
	if serverNameOverride != "" {
		tlsConfig.ServerName = serverNameOverride
	}
	return amqp.DialTLS(m.url, tlsConfig)
}

func (m *RabbitMQ) Close() {
	m.mutex.Lock()

	// close producers
	for _, p := range m.producers {
		p.Close()
	}
	m.producers = m.producers[:0]

	// close consumers
	for _, c := range m.consumers {
		c.Close()
	}
	m.consumers = m.consumers[:0]

	// close mq connection
	select {
	case <-m.stopChan:
		// had been closed
	default:
		close(m.stopChan)
	}

	m.mutex.Unlock()

	// wait done
	for m.State() != StateClosed {
		time.Sleep(time.Second)
	}
}

func (m *RabbitMQ) Producer(name string) (*Producer, error) {
	m.mutex.Lock()
	defer m.mutex.Unlock()

	if m.state != StateOpened {
		return nil, fmt.Errorf("RabbitMQ: Not initialized, now state is %d", m.State())
	}
	p := newProducer(name, m)
	m.producers = append(m.producers, p)
	return p, nil
}

func (m *RabbitMQ) Consumer(name string) (*Consumer, error) {
	m.mutex.Lock()
	defer m.mutex.Unlock()

	if m.state != StateOpened {
		return nil, fmt.Errorf("RabbitMQ: Not initialized, now state is %d", m.State())
	}
	c := newConsumer(name, m)
	m.consumers = append(m.consumers, c)
	return c, nil
}

func (m *RabbitMQ) State() uint8 {
	m.mutex.RLock()
	defer m.mutex.RUnlock()
	return m.state
}

func (m *RabbitMQ) keepalive() {
	select {
	case <-m.stopChan:
		// 正常关闭
		log.Println("[WARN] RabbitMQ: Shutdown normally.")
		m.mutex.Lock()
		m.conn.Close()
		m.state = StateClosed
		m.mutex.Unlock()

	case err := <-m.closeChan:
		if err == nil {
			log.Println("[ERROR] RabbitMQ: Disconnected with RabbitMQ, but Error detail is nil")
		} else {
			if err.Code == 501 {
				log.Fatalf("[ERROR] RabbitMQ: Disconnected with RabbitMQ, code:%d, reason:%s\n", err.Code, err.Reason)
			}
			log.Printf("[ERROR] RabbitMQ: Disconnected with RabbitMQ, code:%d, reason:%s\n", err.Code, err.Reason)
		}
		// tcp连接中断, 重新连接
		m.mutex.Lock()
		m.state = StateReopening
		m.mutex.Unlock()

		maxRetry := 99999999
		for i := 0; i < maxRetry; i++ {
			time.Sleep(time.Second)
			if _, e := m.Open(); e != nil {
				log.Printf("[ERROR] RabbitMQ: Connection recover failed for %d times, %v\n", i+1, e)
				continue
			}
			log.Printf("[INFO] RabbitMQ: Connection recover OK. Total try %d times\n", i+1)
			return
		}
		log.Printf("[ERROR] RabbitMQ: Try to reconnect to RabbitMQ failed over maxRetry(%d), so exit.\n", maxRetry)
	}
}

//func (m *RabbitMQ) Consumer() *Consumer {
//	return nil
//}

func (m *RabbitMQ) connChannel() (*amqp.Channel, error) {
	m.mutex.RLock()
	defer m.mutex.RUnlock()
	return m.conn.Channel()
}

func init() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}
