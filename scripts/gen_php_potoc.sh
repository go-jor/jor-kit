#!/bin/bash -e

#PATH=$PATH:$GOPATH/bin

protodir=../protobuf

protoc --version # 验证 protoc 是否安装成功

protoc -I $protodir $protodir/jorcommon/*.proto  \
  --php_out=../jor-php-api/jorcommon --grpc_out=../jor-php-api/jorcommon \
  --plugin=protoc-gen-grpc=/Users/jimmy/grpc/cmake/build/grpc_php_plugin

#WMS Service
protoc -I $protodir $protodir/wms/*.proto  \
  --php_out=../jor-php-api/jorwms --grpc_out=../jor-php-api/jorwms \
  --plugin=protoc-gen-grpc=/Users/jimmy/grpc/cmake/build/grpc_php_plugin

#Marketplace Service
protoc -I $protodir $protodir/marketplace/*.proto  \
  --php_out=../jor-php-api/jormarketplace --grpc_out=../jor-php-api/jormarketplace \
  --plugin=protoc-gen-grpc=/Users/jimmy/grpc/cmake/build/grpc_php_plugin