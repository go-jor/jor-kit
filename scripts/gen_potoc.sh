#!/bin/bash -e

#PATH=$PATH:$GOPATH/bin

protodir=../protobuf

protoc -I $protodir $protodir/jorcommon/*.proto --go_out=../ --go-grpc_out=../

#WMS Service
protoc -I $protodir $protodir/wms/*.proto  \
--go_opt=Mjorcommon/common.proto=gitlab.com/go-jor/jor-kit/api/jorcommonpb \
--go_out=../ --go-grpc_out=../

#LME Product Service
protoc -I $protodir $protodir/lmeproduct/*.proto  \
--go_opt=Mjorcommon/common.proto=gitlab.com/go-jor/jor-kit/api/jorcommonpb \
--go_out=../ --go-grpc_out=../

#LME OMS Service
protoc -I $protodir $protodir/lmeoms/*.proto  \
--go_opt=Mjorcommon/common.proto=gitlab.com/go-jor/jor-kit/api/jorcommonpb \
--go_out=../ --go-grpc_out=../

#Marketplace Service
protoc -I $protodir $protodir/marketplace/*.proto  \
--go_opt=Mjorcommon/common.proto=gitlab.com/go-jor/jor-kit/api/jorcommonpb \
--go-grpc_opt=Mjorcommon/common.proto=gitlab.com/go-jor/jor-kit/api/jorcommonpb \
--go_out=../ --go-grpc_out=../

#WMS Service
#protoc -I $protodir $protodir/wms/product/*.proto  \
#--go_opt=Mjorcommon/common.proto=gitlab.com/go-jor/jor-kit/api/jorcommonpb \
#--go_out=../api/ --go-grpc_out=../api/