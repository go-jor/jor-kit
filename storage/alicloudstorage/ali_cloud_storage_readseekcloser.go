package alicloudstorage

import (
	"errors"
	"fmt"
	"io"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"
)

type AliCloudStorageReadSeekCloser struct {
	bucket    *oss.Bucket
	objectKey string
	reader    *io.ReadCloser
	index     int64
	fileSize  int64
	whence    int
}

func NewAliCloudStorageReadSeekCloser(bucket *oss.Bucket, objectKey string, fileSize, cacheSize int64) *AliCloudStorageReadSeekCloser {
	readSeeker := &AliCloudStorageReadSeekCloser{
		objectKey: objectKey,
		bucket:    bucket,
		index:     0,
		fileSize:  fileSize,
	}

	reader, err := bucket.GetObject(objectKey)
	if err != nil {
		return nil
	}
	readSeeker.reader = &reader

	return readSeeker
}

func (s *AliCloudStorageReadSeekCloser) Read(p []byte) (n int, err error) {
	if s.index >= s.fileSize {
		err = io.EOF
		s.Close()
		return
	}

	start := s.index - 1
	end := s.index - 1 + int64(len(p))
	if start < 0 {
		start = 0
	}
	if end >= s.fileSize {
		end = s.fileSize - 1
	}

	reader, obErr := s.bucket.GetObject(s.objectKey, oss.Range(start, end))
	if obErr != nil {
		err = obErr
		return
	}
	s.reader = &reader

	rn, _ := (*s.reader).Read(p)
	n = rn
	if n > 0 && s.whence == io.SeekStart {
		s.index += int64(n)
	}

	if cErr := s.Close(); cErr != nil {
		return n, cErr
	}

	if err != nil {
		return n, err
	}

	return
}

func (s *AliCloudStorageReadSeekCloser) Seek(offset int64, whence int) (n int64, err error) {
	s.whence = whence
	switch whence {
	case io.SeekStart:
		n = offset
	case io.SeekCurrent:
		n = s.index + offset
	case io.SeekEnd:
		n = s.fileSize + offset
	default:
		return 0, errors.New(fmt.Sprintf("AliCloudStorageReadSeeker.Seek: invalid whence %d", whence))
	}

	if n < 0 {
		return 0, errors.New(fmt.Sprintf("AliCloudStorageReadSeeker.Seek: negative position %d with offset %d whence %d and fileSize %d", n, offset, whence, s.fileSize))
	}

	indexDiff := n - s.index
	s.index = n

	if indexDiff == 0 {
		return
	}

	return
}

func (s *AliCloudStorageReadSeekCloser) Close() (err error) {
	if s.reader != nil {
		if err = (*s.reader).Close(); err != nil {
			return
		}
		s.reader = nil
	}
	return
}
