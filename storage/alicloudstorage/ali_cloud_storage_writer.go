package alicloudstorage

import (
	"bytes"
	"fmt"
	"log"
	"unicode/utf8"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"
)

type Writer struct {
	Bucket    *oss.Bucket
	ObjectKey string
	nextPos   int64

	err error
}

func NewWriter(bucket *oss.Bucket, objectKey string) *Writer {
	return &Writer{
		Bucket:    bucket,
		ObjectKey: objectKey,
	}
}

func (w *Writer) open() error {
	if !utf8.ValidString(w.ObjectKey) {
		return fmt.Errorf("storage: object name %q is not valid UTF-8", w.ObjectKey)
	}

	return nil
}

func (w *Writer) Write(p []byte) (n int, err error) {
	exist, err := w.Bucket.IsObjectExist(w.ObjectKey)
	if err != nil {
		return
	}
	if exist && w.nextPos == 0 {
		_, err = w.Bucket.CopyObject(w.ObjectKey, "del/"+w.ObjectKey)
		if err != nil {
			return
		}

		err = w.Bucket.DeleteObject(w.ObjectKey)
		if err != nil {
			return
		}
	}
	nextPos, err := w.Bucket.AppendObject(w.ObjectKey, bytes.NewReader(p), w.nextPos)
	n = int(nextPos - w.nextPos)
	w.nextPos = nextPos
	log.Println("put object", w.ObjectKey, err)
	w.err = err
	return
}

func (w *Writer) Close() error {
	return w.err
}
