package cloudstorage

import (
	"context"
	"errors"
	"fmt"
	"io"

	"cloud.google.com/go/storage"
)

type CircularBuffer struct {
	data       []byte
	bufferSize int64
	index      int64
	dataLength int64
}

func NewCircularBuffer(size int64) *CircularBuffer {
	if size <= 0 {
		panic("Size must be positive")
	}
	return &CircularBuffer{
		data:       make([]byte, size),
		bufferSize: size,
	}
}

func (b *CircularBuffer) Len() int64 {
	return b.dataLength
}

func (b *CircularBuffer) Write(p []byte) (n int, err error) {
	pLen := int64(len(p))
	if pLen >= b.bufferSize {
		p = p[pLen-b.bufferSize:]
		pLen = b.bufferSize
	}

	remainingLen := b.bufferSize - b.index
	if pLen <= remainingLen {
		copy(b.data[b.index:], p)
	} else {
		copy(b.data[b.index:], p[:remainingLen])
		copy(b.data, p[remainingLen:])
	}

	n = int(pLen)
	b.index += pLen
	b.index %= b.bufferSize
	b.dataLength += pLen
	if b.dataLength > b.bufferSize {
		b.dataLength = b.bufferSize
	}

	return
}

func (b *CircularBuffer) GetLastBytes(fetchLengh int64) (lastBytes []byte) {
	if fetchLengh > b.dataLength {
		fetchLengh = b.dataLength
	}

	if fetchLengh <= b.index {
		lastBytes = b.data[b.index-fetchLengh : b.index]
	} else {
		lastBytes = append(b.data[b.bufferSize-(fetchLengh-b.index):], b.data[0:b.index]...)
	}
	return
}

func (b *CircularBuffer) Reset() {
	b.index = 0
	b.dataLength = 0
}

type GoogleCloudStorageReadSeekCloser struct {
	objectHandle *storage.ObjectHandle
	ctx          context.Context
	reader       *storage.Reader
	index        int64
	fileSize     int64

	cacheBuffer          *CircularBuffer
	availableCachedBytes []byte
}

func NewGoogleCloudStorageReadSeekCloser(objectHandle *storage.ObjectHandle, ctx context.Context, fileSize, cacheSize int64) *GoogleCloudStorageReadSeekCloser {
	gcsReadSeeker := &GoogleCloudStorageReadSeekCloser{
		objectHandle: objectHandle,
		ctx:          ctx,
		reader:       nil,
		index:        0,
		fileSize:     fileSize,
	}
	if cacheSize > 0 {
		gcsReadSeeker.cacheBuffer = NewCircularBuffer(cacheSize)
	}
	return gcsReadSeeker
}

func (s *GoogleCloudStorageReadSeekCloser) Read(p []byte) (n int, err error) {

	n1 := s.readAvailableCachedBytes(p)
	if n1 > 0 {
		s.writeToCache(p[:n1])
		n += n1
		s.index += int64(n1)
	}
	if n1 >= len(p) {
		return
	}

	if s.index >= s.fileSize {
		err = io.EOF
		s.Close()
		return
	}

	if s.reader == nil {
		s.reader, err = s.objectHandle.NewRangeReader(s.ctx, s.index, -1)
		if err != nil {
			return
		}
	}

	for n < len(p) {
		n2, err := s.reader.Read(p[n:])
		if n2 > 0 {
			s.writeToCache(p[n : n+n2])
			n += n2
			s.index += int64(n2)
		}
		if err == io.EOF {
			s.reader.Close()
		}
		if err != nil {
			return n, err
		}
	}
	return
}

func (s *GoogleCloudStorageReadSeekCloser) Seek(offset int64, whence int) (n int64, err error) {
	switch whence {
	case io.SeekStart:
		n = offset
	case io.SeekCurrent:
		n = s.index + offset
	case io.SeekEnd:
		n = s.fileSize + offset
	default:
		return 0, errors.New(fmt.Sprintf("GoogleCloudStorageReadSeeker.Seek: invalid whence %d", whence))
	}
	if n < 0 {
		return 0, errors.New(fmt.Sprintf("GoogleCloudStorageReadSeeker.Seek: negative position %d with offset %d whence %d and fileSize %d", n, offset, whence, s.fileSize))
	}

	indexDiff := n - s.index
	s.index = n

	if indexDiff == 0 {
		return
	}

	if s.prepareAvailableCachedBytes(indexDiff) {
		return
	}

	if err = s.Close(); err != nil {
		return
	}
	return
}

func (s *GoogleCloudStorageReadSeekCloser) Close() (err error) {
	if s.reader != nil {
		if err = s.reader.Close(); err != nil {
			return
		}
		s.reader = nil
	}
	return
}

func (s *GoogleCloudStorageReadSeekCloser) readAvailableCachedBytes(p []byte) (n int) {
	aLen := len(s.availableCachedBytes)
	if aLen > 0 {
		pLen := len(p)
		if aLen <= pLen {
			copy(p[:aLen], s.availableCachedBytes)
			n = aLen
			s.availableCachedBytes = nil
		} else {
			copy(p, s.availableCachedBytes[:pLen])
			n = pLen
			s.availableCachedBytes = s.availableCachedBytes[n:]
		}
	}
	return
}

func (s *GoogleCloudStorageReadSeekCloser) writeToCache(p []byte) {
	if s.cacheBuffer != nil {
		s.cacheBuffer.Write(p)
	}
}

func (s *GoogleCloudStorageReadSeekCloser) prepareAvailableCachedBytes(indexDiff int64) (success bool) {
	s.availableCachedBytes = nil
	if s.cacheBuffer != nil {
		if indexDiff < 0 && s.cacheBuffer.Len() >= -indexDiff {
			s.availableCachedBytes = s.cacheBuffer.GetLastBytes(-indexDiff)
			success = true
		}
		s.cacheBuffer.Reset()
	}
	return
}
