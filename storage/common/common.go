package common

import "archive/zip"

type BufferContent struct {
	Filename string
	Buffer   []byte
}

type FileContent struct {
	Filename string
	FilePath string
}

type ZipContent struct {
	File          []*FileContent
	BufferContent []*BufferContent // Additional data in the buffer, that was not saved before
	FolderName    string
}

func CopyToZipWriter(zipWriter *zip.Writer, zipContent *ZipContent) error {
	for _, bufferC := range zipContent.BufferContent {
		zipFile, err := zipWriter.Create(bufferC.Filename)
		if err != nil {
			return err
		}
		_, err = zipFile.Write(bufferC.Buffer)
		if err != nil {
			return err
		}
	}
	return nil
}
