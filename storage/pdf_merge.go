package storage

import (
	"bytes"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"log"
	"strings"
	"time"

	"gitlab.com/go-emat/pdfcpu-mattex/pkg/pdfcpu"
	"gitlab.com/go-jor/jor-kit/storage/pdfcpu_helper"
)

const (
	PdfMergeFileTypePDF         = "pdf"
	PdfMergeFileTypeImage       = "image"
	PdfMergeFileTypeUnsupported = "unsupported"
	PdfMergeFileTypeNotExist    = "not_exist"
)

type PdfMergeFileInfo struct {
	FilePath      string
	FileName      string
	FileExtension string
	Appendix      string
	UpdatedAt     *time.Time
}

type PdfWatermarks struct {
	Image string
	Text  string
	Desc  string
	OnTop bool
	Pages []string
}

type PdfWatermarksFileInfo struct {
	FilePath   string
	FileName   string
	Watermarks []*PdfWatermarks
}

type PdfWatermarksFile struct {
	*PdfWatermarksFileInfo
	InputSource io.ReadSeeker
}

type PdfMergeFile struct {
	*PdfMergeFileInfo
	InputSource io.ReadSeeker
	FileType    string
	PdfCtx      *pdfcpu.Context
}

type PdfMergeRequest struct {
	InputFiles     []*PdfMergeFileInfo
	OutputFilePath string
	DocumentNo     string
}

type PdfMergeResponse struct {
	Duration             float64
	SupportedFileNames   []string
	UnsupportedFileNames []string
}

type pdfMergeService struct {
	StorageFactory *DocumentStorageFactory
}

func NewPdfMergeService(storageType, storageBucketName string, configFile ...string) *pdfMergeService {
	return &pdfMergeService{
		StorageFactory: &DocumentStorageFactory{
			StorageType:       storageType,
			StorageBucketName: storageBucketName,
			ConfigFile:        configFile[0],
		},
	}
}

func NewDefaultPdfMergeService() *pdfMergeService {
	return &pdfMergeService{
		StorageFactory: newDefaultDocumentStorageFactory(),
	}
}

func (s *pdfMergeService) MergeFiles(req *PdfMergeRequest) (resp *PdfMergeResponse, errs *pdfcpu_helper.Errors) {
	startTime := time.Now()
	resp = &PdfMergeResponse{}
	errs = pdfcpu_helper.NewErrors()
	defer func() {
		resp.Duration = time.Since(startTime).Seconds()
		if len(errs.Errs) == 0 && len(errs.FatalErrs) == 0 {
			errs = nil
		}
	}()

	if len(req.InputFiles) == 0 {
		errs.AddFatal(errors.New("pdfMergeService.MergeFiles: should provide at least one input file"))
		return
	}

	files, totalUnsupportedFiles, es := s.getFilesAndUnsupportedFiles(req.InputFiles)
	if es != nil {
		errs.Append(es)
	}
	for _, f := range files {
		switch f.FileType {
		case PdfMergeFileTypeUnsupported, PdfMergeFileTypeNotExist:
			resp.UnsupportedFileNames = append(resp.UnsupportedFileNames, f.FileName)
		default:
			resp.SupportedFileNames = append(resp.SupportedFileNames, f.FileName)
		}
	}
	log.Println("validate end", len(files))
	outputSource, err := s.StorageFactory.GetWriteCloser(req.OutputFilePath, "application/pdf")
	if err != nil {
		errs.AddFatal(err)
		return
	}

	defer func() {
		if err := outputSource.Close(); err != nil {
			errs.AddFatal(err)
		}
	}()

	log.Println("createInitialPdfCtxWithAttachments start", "totalUnsupportedFiles", len(totalUnsupportedFiles))
	initPdfCtx, es := s.createInitialPdfCtxWithAttachments(totalUnsupportedFiles)
	if es != nil {
		errs.Append(es)
		return
	}
	log.Println("createInitialPdfCtxWithAttachments end")

	var watermarkTexts []string
	createAndMergePageWithWatermarks := func() {
		if req.DocumentNo != "" {
			watermarkTexts = append(watermarkTexts, fmt.Sprintf("Please find unsupported file(s) in document %v on the website", req.DocumentNo))
		} else {
			watermarkTexts = append(watermarkTexts, "Please find unsupported file(s) in attachments")
		}
		pdfCtx, err := s.createBlankPdfCtxWithWatermark(strings.Join(watermarkTexts, "\n"))
		if err != nil {
			errs.AddFatal(err)
		} else {
			if err := pdfcpu_helper.MergePdfToCtx(initPdfCtx, pdfCtx, "blank_page_with_watermarks"); err != nil {
				errs.AddFatal(err)
			}
		}
		watermarkTexts = []string{}
	}

	createAppendixLeadPage := func(file *PdfMergeFile) {
		pdfCtx, err := s.createAppendixLeadPage(file)
		if err != nil {
			errs.AddFatal(err)
		} else {
			if err := pdfcpu_helper.MergePdfToCtx(initPdfCtx, pdfCtx, "appendix_lead_page"); err != nil {
				errs.AddFatal(err)
			}
		}
	}

	log.Println("Import To Ctx start")

	for _, f := range files {
		log.Println(f.FileName, f.FilePath, f.FileType)
		if f.FileType == PdfMergeFileTypeUnsupported {
			watermarkTexts = append(watermarkTexts, fmt.Sprintf("Cannot merge file %s", f.FileName))
		} else if f.FileType == PdfMergeFileTypeNotExist {
			watermarkTexts = append(watermarkTexts, fmt.Sprintf("Cannot merge nonexistent file %s", f.FileName))
		} else if len(watermarkTexts) > 0 {
			createAndMergePageWithWatermarks()
		}

		if f.FileType == PdfMergeFileTypePDF {
			if f.Appendix != "" {
				createAppendixLeadPage(f)
			}
			if err := pdfcpu_helper.MergePdfToCtx(initPdfCtx, f.PdfCtx, f.FileName); err != nil {
				errs.AddFatal(err)
			}
		}
		if f.FileType == PdfMergeFileTypeImage {
			if f.Appendix != "" {
				createAppendixLeadPage(f)
			}
			if err := pdfcpu_helper.ImportImageToCtx(initPdfCtx, nil, f.InputSource, f.FileName); err != nil {
				errs.AddFatal(err)
			}
		}
	}
	log.Println("Import To Ctx end")
	if len(watermarkTexts) > 0 {
		createAndMergePageWithWatermarks()
	}

	if err := pdfcpu_helper.ValidateAndWriteContext(initPdfCtx, outputSource); err != nil {
		errs.AddFatal(err)
		return
	}

	return
}

func (s *pdfMergeService) GetPdfPageCount(filePath string) (int, error) {
	fileInputSource, err := s.StorageFactory.GetFileReadSeekCloser(filePath)
	if err != nil {
		return 0, errors.New(fmt.Sprintf("pdfMergeService.GetPdfPageCount: %s", err.Error()))
	}

	defer func() {
		if err := fileInputSource.Close(); err != nil {
			return
		}
	}()
	return pdfcpu_helper.PdfInfo(fileInputSource)
}

func (s *pdfMergeService) ValidateMergeFileFormat(inputFile *PdfMergeFileInfo) (f *PdfMergeFile, err error) {
	f = &PdfMergeFile{
		PdfMergeFileInfo: inputFile,
	}
	fileInputSource, err := s.StorageFactory.GetFileReadSeekCloser(inputFile.FilePath)
	if err != nil {
		f.FileType = PdfMergeFileTypeNotExist
		err = pdfcpu_helper.NewFileOpenError(inputFile.FileName, err)
		return f, err
	}
	defer func() {
		if err := fileInputSource.Close(); err != nil {
			return
		}
	}()
	f.InputSource = fileInputSource
	f.FileType = getTypeByExt(strings.ToLower(inputFile.FileExtension))
	log.Println("f.FileType start validate", f.FileType)
	if f.FileType == PdfMergeFileTypePDF {
		pdfCtx, err := pdfcpu_helper.ReadAndValidateInputSource(f.FileName, f.InputSource)

		if err != nil {
			f.FileType = PdfMergeFileTypeUnsupported
			if _, seekErr := f.InputSource.Seek(0, io.SeekStart); seekErr != nil {
				return f, seekErr
			}
			return f, err
		} else {
			f.PdfCtx = pdfCtx
		}
	}
	if f.FileType == PdfMergeFileTypeImage {
		if err := pdfcpu_helper.ValidateImageInputSource(&pdfcpu_helper.ImageFile{
			FileName:    f.FileName,
			InputSource: f.InputSource,
		}); err != nil {
			log.Println("image validate err:", err)
			f.FileType = PdfMergeFileTypeUnsupported
			if _, seekErr := f.InputSource.Seek(0, io.SeekStart); seekErr != nil {
				return f, seekErr
			}
			return f, err
		}
	}
	log.Println("f.FileType after", f.FileType)

	return f, nil
}

func getTypeByExt(extension string) string {
	switch extension {
	case "pdf":
		return PdfMergeFileTypePDF
	case "png", "bmp", "gif", "jpg", "jpeg", "tif", "tiff":
		return PdfMergeFileTypeImage
	default:
		return PdfMergeFileTypeUnsupported
	}
}

func (s *pdfMergeService) getFilesAndUnsupportedFiles(inputFiles []*PdfMergeFileInfo) (files, unsupportedFiles []*PdfMergeFile, errs *pdfcpu_helper.Errors) {
	files, unsupportedFiles = []*PdfMergeFile{}, []*PdfMergeFile{}
	errs = pdfcpu_helper.NewErrors()
	defer func() {
		if len(errs.Errs) == 0 && len(errs.FatalErrs) == 0 {
			errs = nil
		}
	}()
	for _, inputFile := range inputFiles {
		f, err := s.ValidateMergeFileFormat(inputFile)
		log.Println("ValidateMergeFileFormat err", err, f.FileType)
		if err != nil {
			errs.Add(err)
		}
		files = append(files, f)
		if f.FileType == PdfMergeFileTypeUnsupported {
			unsupportedFiles = append(unsupportedFiles, f)
		}
	}
	return
}

func (s *pdfMergeService) createInitialPdfCtxWithAttachments(files []*PdfMergeFile) (pdfCtx *pdfcpu.Context, errs *pdfcpu_helper.Errors) {
	errs = pdfcpu_helper.NewErrors()
	defer func() {
		if len(errs.Errs) == 0 && len(errs.FatalErrs) == 0 {
			errs = nil
		}
	}()

	pdfCtx, err := pdfcpu_helper.NewEmptyContext()
	if err != nil {
		errs.AddFatal(err)
		return
	}
	if len(files) > 0 {
		for _, af := range files {
			a := pdfcpu.Attachment{Reader: af.InputSource, ID: af.FileName, ModTime: af.UpdatedAt}
			if err = pdfCtx.AddAttachment(a, true); err != nil {
				errs.AddFatal(err)
				return
			}
		}
	}
	pdfCtx, err = pdfcpu_helper.WriteContextAndGetNewContext(pdfCtx)
	if err != nil {
		errs.AddFatal(err)
		return
	}
	return
}

func (s *pdfMergeService) createBlankPdfCtxWithWatermark(text string) (pdfCtx *pdfcpu.Context, err error) {
	pdfCtx, err = pdfcpu_helper.NewBlankContext()
	if err != nil {
		return
	}

	wm, err := pdfcpu.ParseTextWatermarkDetails(text, "position:l, rotation:0, scalefactor:1, margins:5", true, pdfcpu.POINTS)
	if err != nil {
		return nil, err
	}
	if err := pdfcpu_helper.AddWatermarksToCtx(pdfCtx, []string{"1"}, wm); err != nil {
		return nil, err
	}
	return
}

func (s *pdfMergeService) AddWatermarksToPdf(req *PdfWatermarksFileInfo) (err error) {

	if req.Watermarks == nil {
		return fmt.Errorf("Watermarks is required")
	}
	fileInputSource, err := s.StorageFactory.GetFileReadSeekCloser(req.FilePath)
	if err != nil {
		err = pdfcpu_helper.NewFileOpenError(req.FileName, err)
		return err
	}
	defer func() {
		if err := fileInputSource.Close(); err != nil {
			return
		}
	}()

	pdfCtx, err := pdfcpu_helper.ReadAndValidateInputSource(req.FileName, fileInputSource)
	if err != nil {
		return err
	}
	for _, watermark := range req.Watermarks {
		wm, err := pdfcpu.ParseTextWatermarkDetails(watermark.Text, watermark.Desc, watermark.OnTop, pdfcpu.POINTS)
		if err != nil {
			return err
		}

		if watermark.Image != "" {
			wm.Mode = pdfcpu.WMImage
			buf := bytes.NewBufferString(watermark.Image)
			dec := base64.NewDecoder(base64.StdEncoding, buf)
			wm.FileName = watermark.Text
			wm.Image = dec
		}

		if err := pdfcpu_helper.AddWatermarksToCtx(pdfCtx, watermark.Pages, wm); err != nil {
			return err
		}
	}

	outputSource, err := s.StorageFactory.GetWriteCloser(req.FilePath, "application/pdf")

	if err != nil {
		return err
	}

	defer func() {
		if err := outputSource.Close(); err != nil {
			return
		}
	}()

	if err := pdfcpu_helper.ValidateAndWriteContext(pdfCtx, outputSource); err != nil {
		return err
	}

	return nil
}

func (s *pdfMergeService) createAppendixLeadPage(file *PdfMergeFile) (*pdfcpu.Context, error) {
	leadPage := pdfcpu.Page{MediaBox: pdfcpu.RectForFormat("A4"), Fm: pdfcpu.FontMap{}, Buf: new(bytes.Buffer)}
	fontName := "Times-Roman"
	k := leadPage.Fm.EnsureKey(fontName)
	td := pdfcpu.TextDescriptor{
		Text:     file.Appendix,
		FontName: fontName,
		FontKey:  k,
		FontSize: 18,
		Scale:    1.0,
		ScaleAbs: true,
		X:        150,
		Y:        540,
	}
	pdfcpu.WriteMultiLine(leadPage.Buf, leadPage.MediaBox, nil, td)

	td = pdfcpu.TextDescriptor{
		Text:     strings.TrimRight(file.FileName, "."+file.FileExtension),
		FontName: fontName,
		FontKey:  k,
		FontSize: 18,
		Scale:    1.0,
		ScaleAbs: true,
		X:        150,
		Y:        500,
	}
	pdfcpu.WriteMultiLine(leadPage.Buf, leadPage.MediaBox, nil, td)

	return pdfcpu_helper.NewContextWithPage(&leadPage)
}
