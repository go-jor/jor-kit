package pdfcpu_helper

import (
	"io"

	"gitlab.com/go-emat/pdfcpu-mattex/pkg/pdfcpu"
)

type PdfFile struct {
	FileName string
	PdfCtx   *pdfcpu.Context
}

type ImageFile struct {
	FileName    string
	InputSource io.ReadSeeker
}
