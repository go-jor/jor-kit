package pdfcpu_helper

import (
	"io"
	"log"
	"strconv"
	"strings"

	pdfcpuApi "gitlab.com/go-emat/pdfcpu-mattex/pkg/api"
	"gitlab.com/go-emat/pdfcpu-mattex/pkg/pdfcpu"
)

func MergePdfToCtx(ctx *pdfcpu.Context, pdfCtx *pdfcpu.Context, pdfFileName string) error {

	ctx.EnsureVersionForWriting()

	if err := pdfcpu.MergeXRefTables(pdfCtx, ctx); err != nil {
		return NewPdfFileMergeError(pdfFileName, err)
	}

	return nil
}

func ReadAndValidateInputSource(fileName string, rs io.ReadSeeker) (pdfCtx *pdfcpu.Context, err error) {
	conf := pdfcpu.NewDefaultConfiguration()
	conf.Cmd = pdfcpu.MERGECREATE

	if pdfCtx, err = pdfcpuApi.ReadContext(rs, conf); err != nil {
		log.Println("pdfcpuApi ReadContext err", err)
		err = NewPdfFileReadError(fileName, err)
		return
	}
	if conf.ValidationMode == pdfcpu.ValidationNone {
		return
	}

	if err = pdfcpuApi.ValidateContext(pdfCtx); err != nil {
		log.Println("pdfcpuApi ValidateContext err", err)

		err = NewPdfFileValidationError(fileName, err)
		return
	}
	return
}

func PdfInfo(rs io.ReadSeeker) (int, error) {
	conf := pdfcpu.NewDefaultConfiguration()
	info, err := pdfcpuApi.Info(rs, nil, conf)
	if err != nil {
		return 0, err
	}
	if len(info) > 0 {
		count, err := strconv.Atoi(strings.TrimSpace(strings.Split(info[1], ":")[1]))
		if err != nil {
			return 0, err
		}
		return count, nil
	}
	return 0, nil
}

func ValidateAndWriteContext(ctxDest *pdfcpu.Context, w io.Writer) (err error) {
	conf := pdfcpu.NewDefaultConfiguration()
	conf.Cmd = pdfcpu.MERGECREATE

	if err = pdfcpuApi.OptimizeContext(ctxDest); err != nil {
		return
	}

	if conf.ValidationMode != pdfcpu.ValidationNone {
		if err = pdfcpuApi.ValidateContext(ctxDest); err != nil {
			return
		}
	}

	if err = pdfcpuApi.WriteContext(ctxDest, w); err != nil {
		return
	}

	return
}
