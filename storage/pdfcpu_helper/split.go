package pdfcpu_helper

import (
	"bytes"
	"io/ioutil"
	"os"

	pdfcpuApi "gitlab.com/go-emat/pdfcpu-mattex/pkg/api"
	"gitlab.com/go-emat/pdfcpu-mattex/pkg/pdfcpu"
)

func SplitPdf(file []byte, fileName string, outPutDir string) error {
	conf := pdfcpu.NewDefaultConfiguration()
	conf.Cmd = pdfcpu.SPLIT

	err := pdfcpuApi.Split(bytes.NewReader(file), outPutDir, fileName,1, conf)
	if err != nil {
		return err
	}
	return nil
}

func MakeSplitDir(dirPath string) error {
	_, err := os.Stat(dirPath)
	if os.IsNotExist(err) {
		err := os.MkdirAll(dirPath, 0755)
		if err != nil {
			return err
		}
	}
	return nil
}

func ReadSplitDir(dirPath string) ([]string, error) {
	files, err := ioutil.ReadDir(dirPath)
	if err != nil {
		return nil, err
	}
	var fileNames []string
	for _, file := range files {
		fileNames = append(fileNames, dirPath + "/" + file.Name())
	}
	return fileNames, nil
}

func RemoveSplitDir(dirPath string) error {
	err := os.RemoveAll(dirPath)
	if err != nil {
		return err
	}
	return nil
}