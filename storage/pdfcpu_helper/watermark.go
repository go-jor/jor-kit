package pdfcpu_helper

import (
	pdfcpuApi "gitlab.com/go-emat/pdfcpu-mattex/pkg/api"
	"gitlab.com/go-emat/pdfcpu-mattex/pkg/pdfcpu"
)

func AddWatermarksToCtx(ctx *pdfcpu.Context, selectedPages []string, wm *pdfcpu.Watermark) error {
	if err := ctx.EnsurePageCount(); err != nil {
		return err
	}

	pages, err := pdfcpuApi.PagesForPageSelection(ctx.PageCount, selectedPages, true)
	if err != nil {
		return err
	}

	if err = ctx.AddWatermarks(pages, wm); err != nil {
		return err
	}

	return nil
}
